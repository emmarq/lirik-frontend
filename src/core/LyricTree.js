import LyricTreeNode from "./LyricTreeNode";
import { textToTree } from ".";

export default class LyricTree {
  constructor() {
    this.root = new LyricTreeNode();
    this.cursor = [];
  }
  dispatch(action) {
    const copy = Object.assign(new LyricTree(), { ...this });

    const obj = copy[action.type].apply(copy, [action.payload]);

    return obj;
  }
  focus(payload) {
    if (this.hasCursor()) {
      const lastFocus = this.findNode(this.cursor);
      if (lastFocus) {
        const copy = lastFocus.copy();
        copy.setFocus(false);
        this.root = copy.getRoot();
      }
    }

    this.cursor = payload.node.getAddress();
    const focused = this.findNode(this.cursor);
    if (focused) {
      const copy = focused.copy();
      copy.setFocus(true);
      this.root = copy.getRoot();
    }
    return this;
  }
  addText(payload) {
    if (this.hasCursor()) {
      payload.text = payload.text.trim();
      if (payload.text.length === 1) {
        const node = this.findNode(this.cursor);
        if (node) {
          const copy = node.copy();

          this.addOneLetter(copy, payload.text);

          this.root = copy.getRoot();
        }
      }
      if (payload.text.length > 1) {
        const node = this.findNode(this.cursor);

        if (node) {
          const copy = node.copy();
          let deep = 5;
          let graft = textToTree(payload.text);

          while (graft) {
            if (graft.next) break;
            deep--;
            graft = graft.mainLeaf;
          }
          if (deep > 0) {
            this.extractRightTree(copy, deep);
            this.cursor = copy.getAddress();
            let highNode = copy;
            for (let d = 0; d < deep; d++) {
              highNode = highNode.nodeRoot;
            }
            const firstGraft = graft;
            let lastGraft = graft;
            while (lastGraft.next) {
              lastGraft.nodeRoot = highNode.nodeRoot;
              lastGraft = lastGraft.next;
            }
            if (highNode.prev) {
              highNode.prev.next = firstGraft;
              firstGraft.prev = highNode.prev;
            } else {
              highNode.nodeRoot.mainLeaf = firstGraft;
            }
            highNode.prev = lastGraft;
            lastGraft.next = highNode;
          } else {
            for (let l = 0; l < payload.text.length; l++) {
              this.addOneLetter(copy, payload.text[l]);
            }
          }
          this.root = copy.getRoot();
        }
      }
    } else if (!this.root.mainLeaf) {
      const text = payload.text.trim();

      const tree = textToTree(text);
      const letter = this.lastLetter(tree);
      letter.setFocus(true);
      this.cursor = letter.getAddress();
      this.root = tree;
    }
    return this;
  }
  handleSpaceKey() {
    if (this.hasCursor()) {
      const node = this.findNode(this.cursor);
      if (node) {
        const copy = node.copy();

        copy.setFocus(false);
        const isFirstToken = copy.nodeRoot.isMainLeaf();
        const isFirstLetter = copy.isMainLeaf();

        if (!isFirstToken || !isFirstLetter) {
          this.extractRightTree(copy); //extraigo el arbol a la derecha del cursor hasta una nueva palabra
        }

        const tokenNode = copy.nodeRoot;
        const wordNode = tokenNode.nodeRoot;
        const prevWord = wordNode.prev;
        if (prevWord) {
          let tNode = prevWord.mainLeaf;
          if (tNode) {
            while (tNode.next) {
              tNode = tNode.next;
            }
            tNode = tNode.copy();

            const newNode = new LyricTreeNode();
            newNode.value = "\u00a0";

            tNode.appendLeaf(newNode);
          }
        }
        copy.setFocus(true);
        this.cursor = copy.getAddress();
        this.root = copy.getRoot();
      }
    }
    return this;
  }
  handleEnterKey() {
    if (this.hasCursor()) {
      const node = this.findNode(this.cursor);

      if (node) {
        const isFirstLine = node.nodeRoot.nodeRoot.nodeRoot.isMainLeaf();
        const isFirstWord = node.nodeRoot.nodeRoot.isMainLeaf();
        const isFirstToken = node.nodeRoot.isMainLeaf();
        const isFirstLetter = node.isMainLeaf();
        if (isFirstLine && isFirstWord && isFirstToken && isFirstLetter)
          return this;

        const copy = node.copy();
        copy.setFocus(false);

        if (isFirstWord && isFirstToken && isFirstLetter) {
          this.extractRightTree(copy, 4);
          //create paragraph
        } else {
          this.extractRightTree(copy, 3);
          //create line
        }
        copy.setFocus(true);
        this.cursor = copy.getAddress();
        this.root = copy.getRoot();
      }
    }
    return this;
  }
  handleBackspaceKey() {
    if (this.hasCursor()) {
      const node = this.findNode(this.cursor);
      if (node) {
        const copy = node.copy();
        copy.setFocus(false);
        const focused = copy.backDelete();
        if (focused) {
          focused.setFocus(true);
          this.cursor = focused.getAddress();
          this.root = focused.getRoot();
        }
      }
    }
    return this;
  }
  handleDeleteKey() {
    if (this.hasCursor()) {
      const node = this.findNode(this.cursor);
      if (node) {
        const copy = node.copy();
        copy.setFocus(false);
        this.root = copy.getRoot();
        const focused = copy.nextDelete();
        if (focused) {
          focused.setFocus(true);
          this.cursor = focused.getAddress();
          this.root = focused.getRoot();
        }
      }
    }
    return this;
  }
  handleLeftKey() {
    if (this.hasCursor()) {
      const node = this.findNode(this.cursor);
      if (node) {
        const copy = node.copy();
        const prevNode = copy.getPrev();
        if (prevNode) {
          const focused = prevNode.copy();
          copy.setFocus(false);
          focused.setFocus(true);
          this.cursor = prevNode.getAddress();
        }
        this.root = copy.getRoot();
      }
    }
    return this;
  }
  handleRightKey() {
    if (this.hasCursor()) {
      const node = this.findNode(this.cursor);
      if (node) {
        const copy = node.copy();
        const nextNode = copy.getNext();
        if (nextNode) {
          const focused = nextNode.copy();
          copy.setFocus(false);
          focused.setFocus(true);
          this.cursor = focused.getAddress();
        }
        this.root = copy.getRoot();
      }
    }
    return this;
  }
  moveLeft(payload) {
    const node = this.findNode(payload.node.getAddress());
    if (node) {
      const copy = node.copy();
      const copyAddress = copy.getAddress();
      const newCursor = copyAddress.join("-") === this.cursor.join("-");
      if (newCursor) copy.setFocus(false);
      const prev = copy.getPrev();
      if (prev) {
        if (!copy.next) {
          copy.nodeRoot.nextDelete();
        } else {
          const nextCopy = copy.next.copy();
          copy.nodeRoot.mainLeaf = nextCopy;
          nextCopy.prev = null;
        }
        const prevCopy = prev.copy();
        prevCopy.append(copy);
        if (newCursor) {
          copy.setFocus(true);
          this.cursor = copy.getAddress();
        }
      }
      this.root = copy.getRoot();
    }
    return this;
  }
  moveRight(payload) {
    const node = this.findNode(payload.node.getAddress());
    if (node && !node.next) {
      const copy = node.copy();
      const copyAddress = copy.getAddress();
      const newCursor = copyAddress.join("-") === this.cursor.join("-");
      if (newCursor) copy.setFocus(false);
      const next = copy.getNext();
      if (next) {
        if (copy.isMainLeaf()) {
          copy.nodeRoot.nextDelete();
        } else {
          const prevCopy = copy.prev.copy();
          prevCopy.next = null;
        }
        const nextCopy = next.copy();
        nextCopy.prepend(copy);
        if (newCursor) {
          copy.setFocus(true);
          this.cursor = copy.getAddress();
        }
      }
      this.root = copy.getRoot();
    }
    return this;
  }
  split(payload) {
    const node = this.findNode(payload.node.getAddress());
    if (node) {
      const copy = node.copy();
      const copyAddress = copy.getAddress();
      const newCursor = copyAddress.join("-") === this.cursor.join("-");
      if (newCursor) copy.setFocus(false);
      const newTokenNode = new LyricTreeNode();
      const prevCopy = copy.prev.copy();
      const prevToken = prevCopy.nodeRoot;
      prevToken.append(newTokenNode);
      newTokenNode.mainLeaf = copy;
      let n = copy;
      while (n) {
        n.nodeRoot = newTokenNode;
        n = n.next;
      }
      prevCopy.next = null;
      copy.prev = null;
      if (newCursor) {
        copy.setFocus(true);
        this.cursor = copy.getAddress();
      }
      this.root = copy.getRoot();
    }
    return this;
  }
  findNodeOnTime(time, node = this.root) {
    if (node.onTime(time)) {
      if (node.hasMainLeaf() && node.mainLeaf.hasMainLeaf())
        return this.findNodeOnTime(time, node.mainLeaf);
      else return node;
    } else {
      if (node.next) return this.findNodeOnTime(time, node.next);
      else return null;
    }
  }
  clearCursor() {
    if (this.hasCursor()) {
      const node = this.findNode(this.cursor);
      if (node) {
        const copy = node.copy();
        copy.setFocus(false);
        this.root = copy.getRoot();
      }
    }
    this.cursor = [];
    return this;
  }
  markInit(payload) {
    let node;
    if (this.hasCursor()) {
      node = this.findNode(this.cursor);
      if (node) {
        node = node.nodeRoot;
        node = node.getNext();
      }
    } else {
      node = this.firstLetter();
      this.cursor = node.getAddress();
      node = node.nodeRoot;
    }
    if (node) {
      node = node.copy();
      node.setInit(payload.time);
      this.focus({ node: node.mainLeaf });
      this.root = node.getRoot();
    }
    return this;
  }
  markEnd(payload) {
    let node;
    if (this.hasCursor()) {
      node = this.findNode(this.cursor);
    } else {
      node = this.firstLetter();
      this.cursor = node.getAddress();
    }
    node = node.nodeRoot;
    node = node.copy();
    node.setEnd(payload.time);
    this.root = node.getRoot();
    return this;
  }
  alignStartTimeOfCurrentWithPrevious() {
    const node = this.findNode(this.cursor);
    if (node) {
      const prev = node.nodeRoot.getPrev();
      if (prev) {
        const copy = node.nodeRoot.copy();
        copy.setInit(prev.end);
        this.root = copy.getRoot();
      }
    }
    return this;
  }
  alignEndTimeOfCurrentWithNext() {
    const node = this.findNode(this.cursor);
    if (node) {
      const next = node.nodeRoot.getNext();
      if (next) {
        const copy = node.nodeRoot.copy();
        copy.setEnd(next.init);
        this.root = copy.getRoot();
      }
    }
    return this;
  }
  increaseStartTimeOfCurrent() {
    const node = this.findNode(this.cursor);
    if (node) {
      const copy = node.nodeRoot.copy();
      copy.setInit(copy.init + 0.1);
      this.root = copy.getRoot();
    }
    return this;
  }
  decreaseStartTimeOfCurrent() {
    const node = this.findNode(this.cursor);
    if (node) {
      const copy = node.nodeRoot.copy();
      copy.setInit(copy.init - 0.1);
      this.root = copy.getRoot();
    }
    return this;
  }
  increaseEndTimeOfCurrent() {
    const node = this.findNode(this.cursor);
    if (node) {
      const copy = node.nodeRoot.copy();
      copy.setEnd(copy.end + 0.1);
      this.root = copy.getRoot();
    }
    return this;
  }
  decreaseEndTimeOfCurrent() {
    const node = this.findNode(this.cursor);
    if (node) {
      const copy = node.nodeRoot.copy();
      copy.setEnd(copy.end - 0.1);
      this.root = copy.getRoot();
    }
    return this;
  }
  changeAllTime(payload) {
    this.root = this.alterTime(payload.factor);
    return this;
  }
  alterTime(factor, node = this.root) {
    const copy = node.simpleCopy();
    copy.init = copy.getInit() + factor;
    copy.end = copy.getEnd() + factor;
    if (copy.hasMainLeaf()) {
      this.alterTime(factor, copy.mainLeaf);
    }
    if (copy.next) {
      this.alterTime(factor, copy.next);
    }
    return copy;
  }
  findNode(address, l = 0, node = this.root) {
    let n = node;
    while (n) {
      if (n.id === address[l]) {
        if (l === address.length - 1) {
          return n;
        } else {
          return this.findNode(address, l + 1, n.mainLeaf);
        }
      }
      n = n.next;
    }
    return null;
  }
  /**PRIVATE METHODS */
  fixRootTime() {
    if ((!this.root.init || !this.root.end) && this.root.mainLeaf) {
      this.root.init = this.root.mainLeaf.init;
      let node = this.root.mainLeaf;
      while (node.next) {
        node = node.next;
      }
      this.root.end = node.end;
    }
  }
  addOneLetter(node, letter) {
    const newNode = new LyricTreeNode();
    newNode.value = letter;
    node.prepend(newNode);
  }
  extractRightTree(node, deep = 2) {
    let highNode = node.nodeRoot;
    const parentNodes = [new LyricTreeNode()];
    for (let i = 1; i < deep; i++) {
      highNode = highNode.nodeRoot;
      parentNodes[i] = new LyricTreeNode(parentNodes[i - 1]);
    }

    highNode.append(parentNodes[0]);

    const newTokenNode = parentNodes[parentNodes.length - 1];
    newTokenNode.mainLeaf = node;

    let transferNode = node;

    if (node.isMainLeaf() && !node.hasMainLeaf()) node.nodeRoot.mainLeaf = null;

    for (let i = 1; i < deep; i++) {
      const mainNode = transferNode.nodeRoot;
      transferNode = transferNode.nodeRoot;
      if (i + 1 < deep && mainNode.isMainLeaf() && !mainNode.hasMainLeaf()) {
        mainNode.nodeRoot.mainLeaf = null;
      }
      if (transferNode.next) {
        const newEquivalentNode = parentNodes[parentNodes.length - 1 - i + 1];

        transferNode.next.prev = newEquivalentNode;
        newEquivalentNode.next = transferNode.next;
        newEquivalentNode.nodeRoot.init = transferNode.init;
        while (transferNode.next) {
          transferNode = transferNode.next;
          transferNode.nodeRoot = newEquivalentNode.nodeRoot;
        }
        newEquivalentNode.nodeRoot.end = transferNode.end;
      }
      mainNode.next = null;
      if (!mainNode.hasMainLeaf() && mainNode.prev) mainNode.prev.next = null;
      transferNode = mainNode;
    }
    newTokenNode.init = node.nodeRoot.init;
    newTokenNode.end = node.nodeRoot.end;
    node.nodeRoot = newTokenNode;
    if (node.prev) node.prev.next = null;
    node.prev = null;
    return parentNodes[0];
  }
  hasCursor() {
    return this.root.mainLeaf && this.cursor.length > 0;
  }
  lastLetter(node) {
    if (node.next) return this.lastLetter(node.next);
    else if (node.mainLeaf) return this.lastLetter(node.mainLeaf);
    else return node;
  }
  firstLetter(node = this.root) {
    let letter = node;
    while (letter.mainLeaf) {
      letter = letter.mainLeaf;
    }
    return letter;
  }
  static adopt(obj) {
    if (obj && obj.constructor.name === "Object") {
      Object.setPrototypeOf(obj, LyricTree.prototype);

      LyricTreeNode.adopt(obj.root);
    }
    return obj;
  }
}
