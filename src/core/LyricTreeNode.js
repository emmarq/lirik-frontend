import shortid from "shortid";

export default class LyricTreeNode {
  constructor(nodeRoot) {
    this.id = shortid.generate();
    if (nodeRoot) nodeRoot.appendLeaf(this);
    this.version = 1;
    this.focus = false;
  }
  prependLeaf(leaf) {
    const newNode = leaf || new LyricTreeNode(this);
    newNode.nodeRoot = this;
    if (this.hasMainLeaf()) {
      newNode.next = this.mainLeaf;
      newNode.next.prev = newNode;
      newNode.prev = null;
      this.mainLeaf = newNode;
    } else {
      this.mainLeaf = newNode;
    }
    return newNode;
  }
  appendLeaf(leaf) {
    const newNode = leaf || new LyricTreeNode(this);
    newNode.nodeRoot = this;
    if (this.hasMainLeaf()) {
      let node = this.mainLeaf;
      while (node.next) {
        node = node.next;
      }
      newNode.setPrev(node);
      newNode.setNext(null);
      node.setNext(newNode);
    } else {
      this.mainLeaf = newNode;
    }
    return newNode;
  }
  append(node) {
    if (this.next) this.next.prev = node;
    node.next = this.next;
    node.prev = this;
    this.next = node;
    node.nodeRoot = this.nodeRoot;
  }
  prepend(node) {
    if (this.prev) this.prev.next = node;
    node.prev = this.prev;
    node.next = this;
    this.prev = node;
    node.nodeRoot = this.nodeRoot;
    if (this.isMainLeaf()) this.nodeRoot.mainLeaf = node;
  }
  moveToNext() {
    if (!this.next && this.nodeRoot.next) {
      if (this.prev) {
        this.prev.next = null;
        this.prev = null;
      }
      if (this.isMainLeaf()) this.nodeRoot.mainLeaf = null;
      this.nodeRoot.next.prependLeaf(this);
    }
  }
  moveToPrev() {
    if (!this.prev && this.nodeRoot.prev) {
      if (this.next) {
        this.next.prev = null;
        this.next = null;
      }
      if (this.isMainLeaf()) this.nodeRoot.mainLeaf = null;
      this.nodeRoot.next.appendLeaf(this);
    }
  }
  nextDelete() {
    let next;
    if (this.next) {
      next = this.next.copy();
      next.prev = this.prev;
    }
    if (this.prev) this.prev.next = this.next;
    if (this.isMainLeaf()) {
      const prev = this.getPrev();
      if (prev) {
        const copyPrev = prev.copy();
        if (next) {
          copyPrev.next = next;
          next.prev = copyPrev;
          let node = next;
          while (node) {
            node.nodeRoot = copyPrev.nodeRoot;
            node = node.next;
          }
        }
      }
    }
    if (!next) {
      next = this.getNext();
      if (next) next = next.copy();
    }
    if (this.isMainLeaf()) {
      this.nodeRoot.mainLeaf = null;
      this.nodeRoot.nextDelete();
    }
    this.prev = null;
    this.next = null;
    this.value = null;
    return next;
  }
  backDelete() {
    let self = this;
    const node = this.getPrev();
    if (node) {
      const deleted = node.copy();
      if (deleted.prev) deleted.prev.next = deleted.next;
      if (deleted.next) deleted.next.prev = deleted.prev;

      const mainLeaf = this.isMainLeaf();

      if (deleted.isMainLeaf()) {
        deleted.nodeRoot.mainLeaf = deleted.next;
        if (!deleted.next) {
          deleted.nodeRoot.nextDelete();
        }
      }

      if (mainLeaf) {
        let topMainLeaf = this.copy();
        self = topMainLeaf;
        while (topMainLeaf.nodeRoot && topMainLeaf.nodeRoot.isMainLeaf()) {
          topMainLeaf = topMainLeaf.nodeRoot;
        }
        topMainLeaf.prev = topMainLeaf.getPrev();
        topMainLeaf.prev = topMainLeaf.prev.copy();
        topMainLeaf.prev.next = topMainLeaf;
        if (topMainLeaf.nodeRoot.prev)
          topMainLeaf.nodeRoot.prev.next = topMainLeaf.nodeRoot.next;
        if (topMainLeaf.nodeRoot.next)
          topMainLeaf.nodeRoot.next.prev = topMainLeaf.nodeRoot.prev;
        let s = topMainLeaf;
        while (s) {
          s.nodeRoot = topMainLeaf.prev.nodeRoot;
          s = s.next;
        }
      }
    }
    return self;
  }
  getInit() {
    if (!this.init) {
      let node = this.mainLeaf;
      while (!this.init && node) {
        this.init = node.init;
        node = node.mainLeaf;
      }
    }
    return this.init;
  }
  getEnd() {
    if (!this.end) {
      let node = this.mainLeaf;
      while (!this.end && node) {
        while (node.next) {
          node = node.next;
        }
        this.end = node.end;
        node = node.mainLeaf;
      }
    }
    return this.end;
  }
  setInit(init) {
    this.init = init;
    let n = this;
    while (n && n.isMainLeaf()) {
      n = n.nodeRoot;
      n.init = init;
    }
  }
  setEnd(end) {
    this.end = end;
    let n = this;
    while (n.nodeRoot && !n.next) {
      n = n.nodeRoot;
      n.end = end;
    }
  }
  compareTime(time) {
    const init = this.getInit();
    const end = this.getEnd();
    const comparison = time < init ? -1 : time > end ? 1 : 0;
    return comparison;
  }
  onTime(time) {
    if (this.getInit() && this.getInit() < time) {
      if (this.getEnd() && this.getEnd() > time) {
        return true;
      }
      const next = this.getNext();
      if (next) {
        if (next.getInit() && next.getInit() > time) {
          return true;
        }
      } else return true;
    }
    return false;
  }
  getNext() {
    if (this.next && this.next.isValid()) {
      return this.next;
    } else {
      let high = 0;
      let sibling = this;
      do {
        while (sibling && !sibling.next) {
          sibling = sibling.nodeRoot;
          high++;
        }
        if (!sibling) break;
        sibling = sibling.next;
        while (sibling && sibling.mainLeaf && high > 0) {
          high--;
          sibling = sibling.mainLeaf;
        }
      } while (high > 0);

      return sibling;
    }
  }
  getPrev() {
    if (this.prev && this.prev.isValid()) return this.prev;
    else {
      let high = 0;
      let sibling = this;
      do {
        while (sibling && !sibling.prev) {
          high++;
          sibling = sibling.nodeRoot;
        }
        if (!sibling) break;
        sibling = sibling.prev;
        while (high > 0 && sibling && sibling.mainLeaf) {
          high--;
          sibling = sibling.mainLeaf;
          while (sibling.next) {
            sibling = sibling.next;
          }
        }
      } while (high > 0);

      return sibling;
    }
  }
  /**
   * debe tener hojas, si no, es hoja y debe tener valor
   */
  isValid() {
    return !!this.mainLeaf || !!this.value;
  }
  setFocus(focus) {
    let node = this;
    while (node) {
      node.focus = focus;
      node = node.nodeRoot;
    }
  }
  isMainLeaf() {
    return (
      this.nodeRoot &&
      this.nodeRoot.mainLeaf &&
      this.nodeRoot.mainLeaf.id === this.id
    );
  }
  hasMainLeaf() {
    return !!this.mainLeaf;
  }
  modify() {}
  setNext(next) {
    this.next = next;
  }
  setPrev(prev) {
    this.prev = prev;
  }
  setMainLeaf(mainLeaf) {
    this.mainLeaf = mainLeaf;
  }
  getRoot() {
    let root = this;
    while (root.nodeRoot) {
      root = root.nodeRoot;
    }
    return root;
  }
  getAddress() {
    const reverse = [],
      address = [];
    let node = this;
    while (node) {
      reverse.push(node.id);
      node = node.nodeRoot;
    }
    for (let i = reverse.length - 1; i >= 0; i--) {
      address.push(reverse[i]);
    }
    return address;
  }
  copy() {
    if (this.nodeRoot) this.nodeRoot = this.nodeRoot.copy();
    const copy = Object.assign(new LyricTreeNode(), { ...this });
    let child = copy.mainLeaf;
    while (child) {
      child.nodeRoot = copy;
      child = child.next;
    }
    if (copy.prev) copy.prev.setNext(copy);
    if (copy.next) copy.next.setPrev(copy);
    if (copy.isMainLeaf()) copy.nodeRoot.setMainLeaf(copy);
    this.setNext(null);
    this.setPrev(null);
    copy.version = this.version + 1;
    return copy;
  }
  simpleCopy() {
    const copy = Object.assign(new LyricTreeNode(), { ...this });
    let child = copy.mainLeaf;
    while (child) {
      child.nodeRoot = copy;
      child = child.next;
    }
    if (copy.prev) copy.prev.setNext(copy);
    if (copy.next) copy.next.setPrev(copy);
    if (copy.isMainLeaf()) copy.nodeRoot.setMainLeaf(copy);
    this.setNext(null);
    this.setPrev(null);
    //copy.version = this.version + 1;
    return copy;
  }
  static adopt(obj) {
    if (obj && obj.constructor.name === "Object") {
      Object.setPrototypeOf(obj, LyricTreeNode.prototype);

      if (!obj.isValid()) {
        if (obj.prev) obj.prev.next = obj.next;
        if (obj.next) obj.next.prev = obj.prev;
      }

      if (obj.mainLeaf) LyricTreeNode.adopt(obj.mainLeaf);
      if (obj.next) LyricTreeNode.adopt(obj.next);
    }
    return obj;
  }
}
