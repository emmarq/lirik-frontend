import { textToTree } from "..";

const text = `
So, so you think you can tell
Heaven from hell
Blue skies from pain
Can you tell a green field
From a cold steel rail?
A smile from a veil?
Do you think you can tell?

Did they get you to trade
Your heroes for ghosts?
Hot ashes for trees?
Hot air for a cool breeze?
Cold comfort for change?
And did you exchange
A walk on part in the war
For a lead role in a cage?
`;

describe("Add text Lyrics", () => {
  it("Add lyrics", () => {
    textToTree(text);
  });
});

describe("Add text Lyrics", () => {
  it("Add lyrics", () => {});

  describe("Serialize/Deserialize lyrics", () => {
    it("Serialize/Deserialize lyrics", () => {});
  });

  describe("spliting token", () => {
    it("Split middle token", () => {});
    it("Split last token", () => {});
    it("Split token when is currentToken", () => {});
    it("Split token when is firstToken", () => {});
    it("Split token when is both current and firstToken", () => {});
  });

  describe("move letters", () => {});
  it("Move letter to right token", () => {});
  it("Move letter to first token", () => {});
  it("Move letter to left current token", () => {});
  it("Move letter to right current token", () => {});
  it("Move letter from first to right current token", () => {});
  it("Move letter from current token to first token", () => {});
  it("Move letter to first and current token", () => {});
});

describe("cursor moving with keys", () => {
  it("move cursor to left letter within same token", () => {});
  it("move cursor to right letter within same token", () => {});
  it("move cursor to left letter with diferent token", () => {});
  it("move cursor to right letter with diferent token", () => {});
  it("move cursor to left letter in first token", () => {});
  it("move cursor to right letter in first token", () => {});
});

describe("cursor moving with mouse", () => {
  it("move cursor", () => {});
});

describe("deleting", () => {
  it("delete backward same token", () => {});
  it("delete backward different token", () => {});
  it("delete backward first token", () => {});
  it("delete forward same token", () => {});
  it("delete forward different token", () => {});
});

describe("marking", () => {
  it("marking init", () => {});
  it("marking end", () => {});
});
