import sonoripy from "talisman/tokenizers/syllables/sonoripy";
import LyricTreeNode from "./LyricTreeNode";

export const textToTree = text => {
  const tree = new LyricTreeNode();

  const paragraphs = text.split(/\n\s*\n/);

  for (let p = 0; p < paragraphs.length; p++) {
    const paragraph = paragraphs[p];
    const lines = paragraph.split(/\n/);
    const paragraphNode = new LyricTreeNode(tree);

    for (let l = 0; l < lines.length; l++) {
      const line = lines[l];
      const words = line.split(/\s/);

      const lineNode = new LyricTreeNode(paragraphNode);

      for (let w = 0; w < words.length; w++) {
        const word = words[w];

        const wordNode = new LyricTreeNode(lineNode);

        const tokens = sonoripy(word);
        let tokenNode;
        for (let t = 0; t < tokens.length; t++) {
          let token = tokens[t];

          tokenNode = new LyricTreeNode(wordNode);
          const tokenSize = token.length;
          if (!tokenSize) {
            continue;
          }

          const lettersSplitted = token.split("");
          for (let i = 0; i < lettersSplitted.length; i++) {
            const letter = lettersSplitted[i];
            const letterNode = new LyricTreeNode(tokenNode);
            letterNode.value = letter;
          }
        }
        const letterNode = new LyricTreeNode(tokenNode);
        letterNode.value = "\u00a0";
      }
    }
  }

  return tree;
};

export const updateModel = old => {
  let token = old.firstToken;
  const root = new LyricTreeNode();
  const paragraph = new LyricTreeNode(root);
  let line = new LyricTreeNode(paragraph);
  let word = new LyricTreeNode(line);
  while (token) {
    if (token.letters[0].letter === "\n") {
      line.init = line.mainLeaf.init;
      line = new LyricTreeNode(paragraph);
      word = new LyricTreeNode(line);
    }
    const tokenNode = new LyricTreeNode(word);
    tokenNode.init = token.init;
    tokenNode.end = token.end;
    for (let l = 0; l < token.letters.length; l++) {
      const letter = token.letters[l].letter;
      if (letter !== "\n") {
        const letterNode = new LyricTreeNode(tokenNode);
        letterNode.value = letter;
      }
    }
    const lastLetter = token.letters[token.letters.length - 1].letter;
    if (lastLetter === "\u00a0" || lastLetter === " ") {
      word.init = word.mainLeaf.init;
      word.end = tokenNode.end;
      line.end = word.end;
      word = new LyricTreeNode(line);
    }
    token = token.next;
  }
  line.init = line.mainLeaf.init;
  paragraph.init = paragraph.mainLeaf.init;
  paragraph.end = line.end;
  return root;
};

const nodeToText = node => {
  let text = "";
  let n = node;
  while (n) {
    if (n.hasMainLeaf()) text += nodeToText(n.mainLeaf);
    else text += n.value;
    n = n.next;
  }
  return text;
};

export const treeToText = node => {
  return nodeToText(node.mainLeaf);
};
