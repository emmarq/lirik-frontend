import React, { Component } from "react";

class Picker extends Component {
	input = React.createRef();
	suggestMode = () => {
		this.setState(state => {
			const nextState = { ...state };
			delete nextState.data.id;
			nextState.form = false;
			return nextState;
		});
	};
	editMode = () => {
		this.setState(state => ({ ...state, form: true }));
	};
	cancel = () => {
		this.props.cancel();
	};
	init = data => {
		this.setState({
			...this.state,
			data: { ...this.state.data, ...(data || {}) }
		});
	};
	editSuggestion = suggestion => {
		this.init(suggestion);
		this.editMode();
	};
	componentDidMount = () => {
		this.input.current.focus();
		this.init(this.props.data);
	};
	componentWillReceiveProps = nextProps => {
		this.init(nextProps.data);
	};
	modeSwitcher = () => {
		return (
			<div>
				<label>
					<input
						type="radio"
						name="suggestion"
						onClick={this.suggestMode}
					/>
					{this.props.t("suggest_mode")}
				</label>

				<label>
					<input
						type="radio"
						name="suggestion"
						onClick={this.editMode}
					/>
					{this.props.t("edit_mode")}
				</label>
			</div>
		);
	};
}

export default Picker;
