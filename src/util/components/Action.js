import React, { Component } from "react";

class Action extends Component {
	triggerAction = e => {
		this.props.fn(this.props.item);
		e.stopPropagation();
	};
	render() {
		const { label } = this.props;
		return <button onClick={this.triggerAction}>{label}</button>;
	}
}

export default Action;
