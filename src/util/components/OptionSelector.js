import React, { Component } from "react";

class OptionSelector extends Component {
  state = { show: false };
  toggle = () => {
    this.setState(state => ({ ...state, show: !state.show }));
  };
  hide = () => {
    this.setState(state => ({ ...state, show: false }));
  };
  select = option => {
    this.setState(state => ({ ...state, show: false }));
    this.props.select(option);
  };
  render = () => {
    const { value, options } = this.props;
    return (
      <div
        tabIndex={0}
        className="select-options"
        onClick={this.toggle}
        onBlur={this.hide}
      >
        {value && value.label}
        <ul className={`options ${this.state.show ? "open" : ""}`}>
          {options.map(option => (
            <Option key={option.label} option={option} select={this.select} />
          ))}
        </ul>
      </div>
    );
  };
}

class Option extends Component {
  select = e => {
    this.props.select(this.props.option);
    e.stopPropagation();
  };
  render = () => {
    return <li onClick={this.select}>{this.props.option.label}</li>;
  };
}

export default OptionSelector;
