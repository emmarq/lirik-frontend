import React, { Component } from "react";

class Modal extends Component {
  render() {
    return this.props.show ? (
      <div className="overlay" style={{ ...this.props.style }}>
        <div className="modal card">
          <div className="card-title">{this.props.title}</div>
          <div className="card-body">{this.props.children}</div>
        </div>
      </div>
    ) : null;
  }
}

export default Modal;
