import React, { Component } from "react";

export const equalityByProp = prop => {
	return (a, b) => {
		return a[prop] === b[prop];
	};
};

const Suggestions = ({ items, pick, k, image, metadata, actions }) => {
	return (
		<div>
			{items.map(item => {
				return (
					<SuggestionItem
						key={item[k]}
						item={item}
						pick={pick}
						image={image && image(item)}
						metadata={metadata && metadata(item)}
						actions={actions && actions(item)}
					/>
				);
			})}
		</div>
	);
};

class SuggestionItem extends Component {
	pick = () => {
		this.props.pick(this.props.item);
	};
	render = () => {
		const { image, metadata, actions } = this.props;
		return (
			<div onClick={this.pick} className="suggestion-item">
				<div className="suggestion-item-image">{image}</div>
				<div className="suggestion-item-metadata">{metadata}</div>
				<div className="suggestion-item-actions">{actions}</div>
			</div>
		);
	};
}

export default Suggestions;
