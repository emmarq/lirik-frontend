import React, { Component } from "react";

class ImageInput extends Component {
	state = { source: "link" };
	uploadFile = () => {};
	changeSource = e => {
		this.setState({ ...this.state, source: e.target.value });
	};
	onChange = e => {
		this.props.onChange({
			target: { id: this.props.id, value: e.target.value }
		});
	};
	render = () => {
		const { model } = this.props;
		return (
			<div className="generic-input">
				<div>
					<label>Imagen</label>
					<select
						onChange={this.changeSource}
						value={this.state.source}
					>
						<option value="file">Archivo</option>
						<option value="link">Enlace</option>
					</select>
				</div>
				{this.state.source === "file" ? (
					<input type="file" onChange={this.onChange} />
				) : (
					<input type="text" onChange={this.onChange} />
				)}

				{model.getErrors("image")}
			</div>
		);
	};
}

export default ImageInput;
