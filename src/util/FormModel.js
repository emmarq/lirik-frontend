import React from "react";

export class FormModel {
  constructor(component) {
    this.component = component;
    this.validations = {};
  }
  addValidation(field, rule, error) {
    this.validations[field] = this.validations[field] || [];
    this.validations[field].push({ rule: rule, error: error });
  }
  validate(data) {
    if (!this.validations) return {};
    data = data || this.component.state.data;
    const errors = {};
    Object.keys(this.validations).forEach(key => {
      this.validations[key].forEach(validation => {
        if (!validation.rule(data[key])) {
          console.log(key, data[key]);
          errors[key] = errors[key] || [];
          errors[key].push(validation.error);
        }
      });
    });
    return errors;
  }
  setField(field, value) {
    this.component.setState(state => {
      const nextState = { ...state };
      nextState.data = { ...state.data, [field]: value };
      if (nextState.hasOwnProperty("valid")) {
        nextState.errors = this.validate(nextState.data);
        nextState.valid =
          Object.getOwnPropertyNames(nextState.errors).length === 0;
      }
      return nextState;
    });
  }
  getField(field) {
    return this.component.state.data[field];
  }
  getErrors(field) {
    return (
      this.component.state.errors &&
      this.component.state.errors[field] &&
      this.component.state.errors[field].map((error, i) => (
        <small key={i}>{error}</small>
      ))
    );
  }
  isValid() {
    if (!this.component.state.hasOwnProperty("valid")) {
      const errors = this.validate();
      const valid = Object.getOwnPropertyNames(errors).length === 0;
      this.component.setState(state => {
        const nextState = { ...state };
        nextState.errors = errors;
        nextState.valid = valid;

        return nextState;
      });

      return valid;
    } else return this.component.state.valid;
  }
  getData() {
    return { ...this.component.state.data };
  }
}

const digitsRegex = /^\d+$/;
export const notEmpty = value => value;
export const digitsOnly = value => digitsRegex.test(value);
