export const origin = process.env.REACT_APP_URI;
export const basename = process.env.REACT_APP_BASENAME;
export const url = url => {
  return process.env.REACT_APP_API_URL + url;
};
export const facebook = {
  redirectUri: process.env.REACT_APP_FACEBOOK_REDIRECT_URI,
  appId: process.env.REACT_APP_FACEBOOK_APP_ID
};

export const google = {
  redirectUri: process.env.REACT_APP_GOOGLE_REDIRECT_URI,
  appId: process.env.REACT_APP_GOOGLE_CLIENT_ID
};

export const providers = { facebook, google };
