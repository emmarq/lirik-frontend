import React from "react";
import styles from "./message.module.css";

const Messages = ({ messages }) => {
  return (
    <div className={styles.messages}>
      <span>
        {messages.map(msg => (
          <Message msg={msg} />
        ))}
      </span>
    </div>
  );
};

const attr = {
  success: {
    style: { color: "#52c41a", marginRight: 5 },
    className: "fas fa-check-circle"
  },
  error: {
    style: { color: "#f5222d", marginRight: 5 },
    className: "fas fa-times-circle"
  }
};

const Message = ({ msg }) => {
  const text = typeof msg.msg === "object" ? msg.msg.message : msg.msg;
  return (
    <div className={styles.message}>
      <div className={styles.content}>
        <i {...attr[msg.type]} />
        <span>{text}</span>
      </div>
    </div>
  );
};

export default Messages;
