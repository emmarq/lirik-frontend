const error = "error";
const success = "success";

const message = function() {
  this.queue = [];
  this.subscribers = [];
  this.notify = () => {
    for (let s = 0; s < this.subscribers.length; s++) {
      this.subscribers[s].apply(null, [this.queue]);
    }
  };
};

message.prototype.append = function(msg) {
  this.queue.push(msg);
  this.notify();
  setTimeout(() => {
    this.remove(msg);
  }, msg.duration * 1000);
};
message.prototype.remove = function(msg) {
  for (let i = 0; i < this.queue.length; i++) {
    if (this.queue[i].msg === msg.msg) {
      this.queue.splice(i, 1);
      break;
    }
  }
  this.notify();
};
message.prototype.success = function(msg, duration = 5) {
  this.append({ type: success, msg: msg, duration: duration });
};
message.prototype.error = function(msg, duration = 5) {
  this.append({ type: error, msg: msg, duration: duration });
};
message.prototype.subscribe = function(subscriber) {
  this.subscribers.push(subscriber);
};

export default new message();
