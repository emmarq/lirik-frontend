import React from "react";
import { Suggestions, Picker } from "../../util/components";
import { getGenres } from "./Service";
import { withTranslation } from "react-i18next";

class GenrePicker extends Picker {
  input = React.createRef();
  state = { data: {}, errors: {}, genres: [] };
  onChange = e => {
    const name = e.target.value;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (!name) {
        this.setState(state => ({ ...state, genres: [] }));
        return;
      }
      getGenres(`search=${name}&per_page=5`).then(page => {
        console.log(page);
        this.setState(state => {
          return { ...state, genres: page.entries };
        });
      });
    }, 250);
    this.setState(state => ({ ...state, data: { name: name } }));
  };
  itemImage = item => {
    return item.image && <img src={item.image} alt={item.name} />;
  };

  itemMetadata = item => {
    return <div>{item.name}</div>;
  };
  render = () => {
    return (
      <div>
        <div className="generic-input">
          <label htmlFor="name">{this.props.t("name")}</label>
          <input
            ref={this.input}
            id="name"
            type="text"
            value={this.state.data.name}
            onChange={this.onChange}
          />
        </div>
        <Suggestions
          items={this.state.genres}
          pick={this.props.pick}
          k="id"
          image={this.itemImage}
          metadata={this.itemMetadata}
        />
      </div>
    );
  };
}

export default withTranslation()(GenrePicker);
