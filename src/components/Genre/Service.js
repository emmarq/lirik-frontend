import { url } from "../../util/variables";

export const getGenres = queryPart => {
  return fetch(url(`/api/genres?${queryPart}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    return response.json().then(error => Promise.reject(error.message));
  });
};

export const getGenre = genreId => {
  return fetch(url(`/api/genres/${genreId}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    else return Promise.reject("La genrea no existe");
  });
};
