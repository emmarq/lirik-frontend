import React, { Component } from "react";
import ReactPlayer from "react-player";
import Duration from "./Duration";
import styles from "./styles.module.css";

class Player extends Component {
  constructor(props) {
    super(props);
    this.state = { playing: false, played: 0, duration: 0, playedSeconds: 0 };
    this.player = React.createRef();
    this.getReactPlayer = this.getReactPlayer.bind(this);
    this.onProgress = this.onProgress.bind(this);
    this.onPlay = this.onPlay.bind(this);
    this.onPause = this.onPause.bind(this);
    this.onDuration = this.onDuration.bind(this);
  }
  getReactPlayer() {
    return this.player.current;
  }
  onDuration(duration) {
    if (this.state.playedSeconds && !this.state.duration) {
      this.player.current.seekTo(this.state.playedSeconds);
    }
    this.setState({
      ...this.state,
      played: this.state.playedSeconds / duration,
      duration: duration
    });
  }
  onProgress(data) {
    this.props.onProgress && this.props.onProgress(data);
    if (this.state.playedSeconds + 1 < data.playedSeconds) {
      this.setState({ ...this.state, ...data });
    }
  }
  onPlay() {
    this.setState({ ...this.state, playing: true });
  }
  onPause() {
    this.setState({ ...this.state, playing: false });
  }
  onSeekMouseDown = e => {
    this.setState({ ...this.state, seeking: true });
  };
  onSeekChange = e => {
    const playedSeconds = e.target.value * this.state.duration;
    this.props.onProgress &&
      this.props.onProgress({
        playedSeconds: playedSeconds
      });
    this.setState({ ...this.state, played: e.target.value });
    this.seekTo(playedSeconds);
  };
  /**
   * seek to always should take seconds
   *
   * @param {seconds} time
   */
  seekTo(time) {
    this.player.current.seekTo(time);
    /*if (time < 1) {
      this.props.onProgress &&
        this.props.onProgress({
          playedSeconds: this.state.duration * time
        });
      this.setState({
        ...this.state,
        playedSeconds: this.state.duration * time,
        played: time,
        seeking: false
      });
    } else {*/
    this.props.onProgress && this.props.onProgress({ playedSeconds: time });
    this.setState({
      ...this.state,
      playedSeconds: time,
      played: time / this.state.duration,
      seeking: false
    });
    //}
  }
  getCurrentTime() {
    return this.player.current.getCurrentTime();
  }
  render() {
    return (
      <div className={styles.playerWrapper}>
        <div className={styles.player} style={this.props.style}>
          <ReactPlayer
            ref={this.player}
            width="100%"
            height="100%"
            progressInterval={50}
            url={this.props.url}
            playing={this.props.playing}
            onDuration={this.onDuration}
            onProgress={this.onProgress}
            onPlay={this.onPlay}
            onPause={this.onPause}
          />
        </div>
        <div className={styles.playerBar}>
          <button
            className={styles.playPause}
            onClick={this.props.playPauseClick}
          >
            {this.state.playing ? (
              <i className="fas fa-pause" />
            ) : (
              <i className="fas fa-play" />
            )}
          </button>
          <div className={styles.playBar}>
            <input
              className={styles.slider}
              type="range"
              min={0}
              max={1}
              step="any"
              value={this.state.played}
              onMouseDown={this.onSeekMouseDown}
              onChange={this.onSeekChange}
              onMouseUp={this.onSeekMouseUp}
            />
          </div>
          <div className={styles.playTime}>
            <Duration seconds={this.state.duration * this.state.played} /> /{" "}
            <Duration seconds={this.state.duration} />
          </div>
        </div>
      </div>
    );
  }
}

export default Player;
