import React from "react";
import { Link } from "react-router-dom";
import styles from "./home.module.css";

const Home = props => (
  <div className={styles.homePage}>
    <div className={styles.landing}>
      <div>
        <h2>¡Bienvenido a Lirik!</h2>
        <input
          className={styles.landingSearch}
          type="text"
          placeholder="Busca una canción"
        />
        <p className={styles.landingText}>
          <Link to="/interpretations">Descubre</Link> o
          <Link to="/songs/new"> crea</Link> pistas de canciones
        </p>
      </div>
    </div>
    {/*
    <div>
      <div>
        <h3>Aprende un idioma</h3>
        <p>
          Millones de personas han aprendido otro lenguaje escuchando y cantando
          en otro idioma. Tu tambien puedes, empieza con lirik.
        </p>
      </div>
      <div>
        <h3>Diviertete</h3>
        <p>
          Busca tus canciones favoritas, canta y sorprende a tus amigos. (o en
          privado, como quieras)
        </p>
      </div>
    </div>
  */}
  </div>
);

export default Home;
