import { url, facebook, google, providers } from "../../util/variables";
import shortid from "shortid";
import { origin } from "../../util/variables";

export const facebookRequest = state => {
  return (
    "https://www.facebook.com/v3.2/dialog/oauth?" +
    "client_id=" +
    facebook.appId +
    "&redirect_uri=" +
    facebook.redirectUri +
    "&state=" +
    state +
    "&scope=email"
  );
};

export const googleRequest = state => {
  return (
    "https://accounts.google.com/o/oauth2/v2/auth?" +
    "client_id=" +
    google.appId +
    "&redirect_uri=" +
    google.redirectUri +
    "&state=" +
    state +
    "&scope=email&response_type=code"
  );
};

export const checkJWT = () => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject();
  return fetch(url("/jwt-valid"), {
    headers: { Authorization: `Bearer ${jwt}` }
  }).then(response => {
    if (response.status !== 200) {
      localStorage.removeItem("jwt");
      return Promise.reject("No token");
    } else return response.json();
  });
};

export const validateOauth = data => {
  return fetch(url("/authenticate"), {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      code: data.code,
      redirectUri: providers[data.provider].redirectUri,
      provider: data.provider
    })
  }).then(response => {
    if (response.status === 200) {
      const token = response.headers.get("Authorization").slice(7);
      localStorage.setItem("jwt", token);
      return response.json().then(user => {
        return { token: token, user: user };
      });
    }
    return Promise.reject("Authentication failed");
  });
};

class OAuthVerifier {
  constructor() {
    window.addEventListener("message", this.authCallback, false);
  }
  state = shortid.generate();
  facebookLogin = () => {
    window.open(facebookRequest(this.state));
  };
  googleLogin = () => {
    window.open(googleRequest(this.state));
  };
  twitterLogin = () => {};
  authCallback = e => {
    if (e.origin === origin && e.data.state === this.state)
      validateOauth(e.data)
        .then(jwt => {
          this.onAuth && this.onAuth(jwt);
        })
        .catch(e => {
          this.onFailure && this.onFailure();
        });
  };
  twitterCallback = () => {};
}

export const oauthVerifier = new OAuthVerifier();
