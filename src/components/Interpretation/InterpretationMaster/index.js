import React, { useState, useEffect } from "react";
import { NavLink, Switch, Route } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { getInterpretation, prepareInterpretation } from "../Service";
import JSOG from "jsog";
import {
	EditInterpretation,
	Interpretation,
	InterpretationLyrics
} from "../index";

function useInterpretation(initial) {
	const [state, setState] = useState(initial);

	function setInterpretation(arg) {
		let interpretation = arg;
		if (typeof arg === "function") {
			interpretation = arg(state);
		}
		if (!interpretation.id) {
			const payload = { ...interpretation };
			payload.serialized = payload.serialized
				? JSOG.stringify(payload.serialized)
				: null;
			localStorage.setItem("interpretation", JSON.stringify(payload));
		} else {
			localStorage.removeItem("interpretation");
		}
		setState(interpretation);
	}

	return [state, setInterpretation];
}

const InterpretationMaster = props => {
	const { t } = useTranslation();
	const [interpretation, setInterpretation] = useInterpretation({});
	useEffect(() => {
		if (props.match.params.id !== "new") {
			getInterpretation(props.match.params.id).then(interpretation => {
				setInterpretation(interpretation);
			});
		} else {
			const cache = localStorage.getItem("interpretation");
			console.log(cache);
			setInterpretation(
				prepareInterpretation(
					cache
						? JSON.parse(cache)
						: {
								id: null,
								name: "",
								description: "",
								year: "",
								lang: "english",
								interpretation_performers: [],
								interpretation_genres: [],
								album_interpretations: [],
								song: {},
								song_id: "0"
						  }
				)
			);
		}
	}, [props.match.params.id]);
	return (
		<div className="container">
			<div>
				<h3 style={{ float: "left", margin: 0, marginTop: 12 }}>
					{interpretation.name}
				</h3>
				<ul className="tabs">
					<li className="tab">
						<NavLink
							to={`${props.match.url}/lyrics`}
							activeClassName="active"
							exact
						>
							{t("sync_interpretation")}
						</NavLink>
					</li>
					<li className="tab">
						<NavLink
							to={`${props.match.url}/edit`}
							activeClassName="active"
							exact
						>
							{t("edit_interpretation")}
						</NavLink>
					</li>
					<li className="tab">
						<NavLink
							to={`${props.match.url}`}
							activeClassName="active"
							exact
						>
							{t("interpretation")}
						</NavLink>
					</li>
					<li style={{ clear: "both" }} />
				</ul>
			</div>
			<Switch>
				<Route
					path={`${props.match.path}/edit`}
					render={props => (
						<EditInterpretation
							{...props}
							interpretation={interpretation}
							setInterpretation={setInterpretation}
						/>
					)}
				/>
				<Route
					path={`${props.match.path}/lyrics`}
					render={props => {
						return interpretation.serialized ? (
							<InterpretationLyrics
								{...props}
								interpretation={interpretation}
								setInterpretation={setInterpretation}
							/>
						) : null;
					}}
				/>
				<Route
					path={`${props.match.path}`}
					render={props => {
						return interpretation.serialized ? (
							<Interpretation
								{...props}
								interpretation={interpretation}
								setInterpretation={setInterpretation}
							/>
						) : null;
					}}
				/>
			</Switch>
		</div>
	);
};

export default InterpretationMaster;
