import React, { Component } from "react";
import InterpretationForm from "../InterpretationForm";
import { createInterpretation } from "../Service";
import { getSong } from "../../Song/Service";
import styles from "./new-interpretation.module.css";
import message from "../../Message/state";
import { withTranslation } from "react-i18next";

class NewInterpretation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.createInterpretation = this.createInterpretation.bind(this);
    this.cancel = this.cancel.bind(this);
  }
  createInterpretation(interpretation) {
    createInterpretation(interpretation)
      .then(created => {
        message.success(this.props.t("interpretation_created"));
        this.props.history.push(`/interpretations/${created.id}`);
      })
      .catch(e => {
        message.error(e);
      });
  }
  cancel() {
    this.props.history.push(`/`);
  }
  componentDidMount() {
    getSong(this.props.match.params.id).then(song => {
      this.setState({ song: song });
    });
  }
  render() {
    return (
      <div className={`${styles.newInterpretation} container card`}>
        <div className="card-title">
          <h2>{this.props.t("new_interpretation")}</h2>
        </div>
        <div className="card-body">
          <InterpretationForm
            data={
              this.state.song
                ? { song: this.state.song, song_id: this.state.song.id }
                : {}
            }
            submit={this.createInterpretation}
            cancel={this.cancel}
            submitLabel={this.props.t("create_interpretation")}
            cancelLabel={this.props.t("cancel")}
          />
        </div>
      </div>
    );
  }
}

export default withTranslation()(NewInterpretation);
