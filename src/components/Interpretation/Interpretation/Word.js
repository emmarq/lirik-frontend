import React, { Component } from "react";
import Token from "./Token";

class Word extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    const before = nextProps.node.compareTime(this.props.time);
    const after = nextProps.node.compareTime(nextProps.time);
    const changeInside = after === 0;
    if (this.props.node.id === "9lnHscGE4bf")
      console.log(
        this.props.node.init,
        nextProps.time,
        this.props.node.end,
        before,
        after
      );
    return (
      nextProps.node.version !== this.props.node.version ||
      (nextProps.time !== this.props.time && (changeInside || before !== after))
    );
  }
  render() {
    //to quote with this times, remember!
    if (
      this.props.startTime &&
      this.props.endTime &&
      (this.props.endTime < this.props.node.init ||
        this.props.startTime > this.props.node.end)
    )
      return null;
    const tokens = [];
    let tokenNode = this.props.node.mainLeaf;
    while (tokenNode) {
      tokens.push(
        <Token
          startTime={this.props.startTime}
          endTime={this.props.endTime}
          key={tokenNode.id}
          time={this.props.time}
          node={tokenNode}
          seekTo={this.props.seekTo}
        />
      );
      tokenNode = tokenNode.next;
    }
    return tokens;
  }
}

export default Word;
