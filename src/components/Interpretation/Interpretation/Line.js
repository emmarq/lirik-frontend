import React, { Component } from "react";
import Word from "./Word";

class Line extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    const before = nextProps.node.compareTime(this.props.time);
    const after = nextProps.node.compareTime(nextProps.time);
    const changeInside = after === 0;
    return (
      nextProps.node.version !== this.props.node.version ||
      (nextProps.time !== this.props.time && (changeInside || before !== after))
    );
  }
  render() {
    //to quote with this times, remember!
    if (
      this.props.startTime &&
      this.props.endTime &&
      (this.props.endTime < this.props.node.init ||
        this.props.startTime > this.props.node.end)
    )
      return null;
    const words = [];
    let wordNode = this.props.node.mainLeaf;
    while (wordNode) {
      words.push(
        <Word
          startTime={this.props.startTime}
          endTime={this.props.endTime}
          key={wordNode.id}
          time={this.props.time}
          node={wordNode}
          seekTo={this.props.seekTo}
        />
      );
      wordNode = wordNode.next;
    }
    return <div>{words}</div>;
  }
}

export default Line;
