import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import Player from "../../Player";
import _ from "lodash";
import styles from "./interpretation.module.css";
import queryString from "query-string";
import Paragraph from "./Paragraph";
import { withTranslation } from "react-i18next";

class Interpretation extends PureComponent {
  state = { fetched: false, playing: false, lapse: {} };
  player = React.createRef();
  onClick = e => {
    this.setState(prevState => {
      const nextState = { ...prevState };
      nextState.playing = !nextState.playing;
      return nextState;
    });
  };
  onProgress = _.throttle(
    data => {
      this.setState(prevState => {
        const nextState = { ...prevState };
        nextState.timeEllapsed = data.playedSeconds;
        if (
          this.state.lapse &&
          !this.state.lapse.triggered &&
          this.state.lapse.b &&
          data.playedSeconds > this.state.lapse.b.init
        ) {
          nextState.playing = false;
          nextState.lapse.triggered = true;
        }
        return nextState;
      });
    },
    100,
    {
      trailing: false
    }
  );
  seekTo = token => {
    this.player.current.seekTo(token.init - 0.333);
    const lapse = this.state.lapse ? { ...this.state.lapse } : {};
    if (!lapse.a) {
      lapse.a = token;
      delete lapse.b;
    } else if (!lapse.b) {
      if (lapse.a.init > token.init) {
        lapse.b = lapse.a;
        lapse.a = token;
      } else lapse.b = token;
    } else if (lapse.b) {
      lapse.a = token;
      delete lapse.b;
    }
    this.setState(prevState => {
      const nextState = { ...prevState };
      nextState.lapse = lapse;
      return nextState;
    });
    this.props.history.replace(
      this.props.match.url +
        "?a=" +
        (lapse.a.init + 0.01).toFixed(3) +
        (lapse.b ? "&b=" + (lapse.b.init + 0.01).toFixed(3) : "")
    );
  };
  componentDidMount = () => {
    this.setState(prevState => {
      const nextState = { ...this.state };
      const queryPart = this.props.location.search.slice(1);
      const params = queryString.parse(queryPart);
      if (params.a) {
        const timeA = parseFloat(params.a) + 0.01;
        nextState.lapse = {
          a: this.props.interpretation.serialized.findNodeOnTime(timeA)
        };
        nextState.timeEllapsed = timeA - 0.333;
        this.player.current.seekTo(nextState.timeEllapsed);
      }
      if (params.b) {
        const timeB = parseFloat(params.b) + 0.01;
        nextState.lapse.b = this.props.interpretation.serialized.findNodeOnTime(
          timeB
        );
        nextState.playing = true;
      }

      nextState.fetched = true;
      return nextState;
    });
  };
  componentDidUpdate(prevProps, prevState) {
    //console.log(prevProps, prevState, this.props, this.state);
  }
  render = () => {
    const interpretation = this.props.interpretation;
    return (
      <div className={`${styles.interpretation}`}>
        <InterpretationLyrics
          interpretation={interpretation}
          time={this.state.timeEllapsed}
          seekTo={this.seekTo}
        />
        <InterpretationMedia
          interpretation={interpretation}
          playing={this.state.playing}
          onProgress={this.onProgress}
          playPauseClick={this.onClick}
          player={this.player}
        />
        <InterpretationMetadata interpretation={interpretation} />
        <InterpretationQuote
          interpretation={interpretation}
          time={this.state.timeEllapsed}
          seekTo={this.seekTo}
          start={this.state.lapse.a}
          end={this.state.lapse.b}
        />
      </div>
    );
  };
}

const InterpretationMetadata = React.memo(({ interpretation }) =>
  !interpretation ? (
    "wait"
  ) : (
    <div className={`${styles.metadata} card`}>
      <div className="card-title">{interpretation && interpretation.name}</div>
      <div className="card-body">
        <div className="card-meta">
          <div className="card-meta-avatar">
            <span className="avatar">
              <img src="" alt="avatar" />
            </span>
          </div>
          <div className="card-meta-detail">
            <div className="card-meta-title" />
            <div className="card-meta-description">
              <div>
                <Link to={`/songs/${interpretation.song_id}`}>
                  {interpretation.song.name}
                </Link>
              </div>
              {interpretation.artist} &middot; {interpretation.year}
            </div>
          </div>
        </div>
        {interpretation.description}
      </div>
      <ul className="actions">
        <li>
          <Link
            style={{ margin: 10 }}
            to={`/interpretations/${interpretation.id}/edit`}
          >
            <i className="fas fa-edit" />
          </Link>
        </li>
        <li>
          <Link
            style={{ margin: 10 }}
            to={`/interpretations/${interpretation.id}/lyrics`}
          >
            <i className="fas fa-font" />
          </Link>
        </li>
      </ul>
    </div>
  )
);

const InterpretationMedia = React.memo(
  ({ interpretation, playing, onProgress, playPauseClick, player }) => (
    <div className={`${styles.media} card`}>
      <Player
        style={{ height: 252 }}
        url={interpretation ? interpretation.media : ""}
        playing={playing}
        onProgress={onProgress}
        playPauseClick={playPauseClick}
        ref={player}
      />
    </div>
  )
);

const InterpretationLyrics = ({ interpretation, time, seekTo }) => {
  if (!interpretation) return null;
  const paragraphs = [];
  let paragraph = interpretation.serialized.root.mainLeaf;
  while (paragraph) {
    paragraphs.push(paragraph);
    paragraph = paragraph.next;
  }
  return (
    <div className={`${styles.lyrics} card`}>
      <div className="card-body">
        {paragraphs.map(paragraph => (
          <Paragraph
            key={paragraph.id}
            node={paragraph}
            time={time}
            seekTo={seekTo}
          />
        ))}
      </div>
    </div>
  );
};

class InterpretationQuote extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    const { time, start, end, interpretation } = nextProps;
    return (
      !!interpretation ||
      (!!start && !!end && time > start.init && time < end.end)
    );
  }
  render() {
    const { interpretation, time, seekTo, start, end } = this.props;
    if (!interpretation || !start || !end) {
      return null;
    }
    const paragraphs = [];
    let paragraph = interpretation.serialized.root.mainLeaf;
    while (paragraph) {
      paragraphs.push(paragraph);
      paragraph = paragraph.next;
    }
    return (
      <div className={`${styles.quote} card`}>
        <div className="card-title">Fragmento</div>
        <div className="card-body">
          {paragraphs.map(paragraph => (
            <Paragraph
              startTime={start.init}
              endTime={end.end}
              key={paragraph.id}
              node={paragraph}
              time={time}
              seekTo={seekTo}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default withTranslation()(Interpretation);
