import React, { Component } from "react";
import Line from "./Line";

class Paragraph extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    const before = nextProps.node.compareTime(this.props.time);
    const after = nextProps.node.compareTime(nextProps.time);
    const changeInside = after === 0;
    return (
      nextProps.node.version !== this.props.node.version ||
      (nextProps.time !== this.props.time && (changeInside || before !== after))
    );
  }
  render() {
    //to quote with this times, remember!
    if (
      this.props.startTime &&
      this.props.endTime &&
      (this.props.endTime < this.props.node.init ||
        this.props.startTime > this.props.node.end)
    )
      return null;

    const lines = [];
    let lineNode = this.props.node.mainLeaf;
    while (lineNode) {
      lines.push(
        <Line
          startTime={this.props.startTime}
          endTime={this.props.endTime}
          key={lineNode.id}
          time={this.props.time}
          node={lineNode}
          seekTo={this.props.seekTo}
        />
      );
      lineNode = lineNode.next;
    }
    return <div>{lines}</div>;
  }
}

export default Paragraph;
