import React, { Component } from "react";

class Token extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    const before = nextProps.node.compareTime(this.props.time);
    const after = nextProps.node.compareTime(nextProps.time);
    const changeInside = after === 0;
    return (
      nextProps.node.version !== this.props.node.version ||
      (nextProps.time !== this.props.time && (changeInside || before !== after))
    );
  }
  seekTo = () => {
    this.props.seekTo(this.props.node);
  };
  render() {
    //to quote with this times, remember!
    if (
      this.props.startTime &&
      this.props.endTime &&
      (this.props.endTime < this.props.node.init ||
        this.props.startTime > this.props.node.end)
    )
      return null;
    const check = this.props.time > this.props.node.init;
    let letters = "";
    let letterNode = this.props.node.mainLeaf;
    while (letterNode) {
      letters += letterNode.value;

      letterNode = letterNode.next;
    }
    return (
      <span onClick={this.seekTo} style={check ? { color: "red" } : {}}>
        {letters}
      </span>
    );
  }
}

export default Token;
