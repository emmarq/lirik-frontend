import { url } from "../../util/variables";
import LyricTree from "../../core/LyricTree";
import JSOG from "jsog";
import { textToTree, treeToText } from "../../core";

export const prepareInterpretation = interpretation => {
  const obj =
    interpretation.serialized && JSOG.parse(interpretation.serialized);
  let serialized;
  if (!obj) {
    serialized = new LyricTree();
    if (interpretation.lyrics) {
      serialized.root = textToTree(interpretation.lyrics);
    }
  } else {
    serialized = LyricTree.adopt(obj);
  }
  interpretation.serialized = serialized;
  interpretation.serialized.fixRootTime();
  return interpretation;
};

export const getInterpretations = queryPart => {
  return fetch(url(`/api/interpretations?${queryPart}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    return response.json().then(error => Promise.reject(error.message));
  });
};

export const createInterpretation = interpretation => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");

  const payload = { ...interpretation };
  const text = payload.serialized ? treeToText(payload.serialized.root) : "";
  payload.lyrics = text;
  payload.serialized = payload.serialized
    ? JSOG.stringify(payload.serialized)
    : null;
  delete payload.changed;

  return fetch(url(`/api/songs/${interpretation.song_id}/interpretations`), {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(payload)
  })
    .then(response => {
      if (response.status === 200) {
        localStorage.removeItem("interpretation");
        return response.json();
      }
      return Promise.reject();
    })
    .then(prepareInterpretation);
};

export const getInterpretation = interpretationId => {
  return fetch(url(`/api/interpretations/${interpretationId}`), {
    headers: { Accept: "application/json" }
  })
    .then(response => {
      if (response.status === 200) {
        return response.json();
      }
      return Promise.reject();
    })
    .then(prepareInterpretation);
};

export const updateInterpretation = interpretation => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  const payload = { ...interpretation };
  const text = treeToText(payload.serialized.root);
  payload.lyrics = text;
  payload.serialized = JSOG.stringify(payload.serialized);
  delete payload.changed;
  delete payload.song;
  delete payload.song_id;

  return fetch(url(`/api/interpretations/${interpretation.id}`), {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(payload)
  })
    .then(response => {
      if (response.status === 200) return response.json();
      return Promise.reject();
    })
    .then(prepareInterpretation);
};
