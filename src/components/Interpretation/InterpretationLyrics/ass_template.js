const template = (font, size, orientation) => `[Script Info]
PlayResX: 1280
PlayResY: 720

[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR,	 MarginV, Encoding
Style: Default,${font},${size},&H0000FFFF,&H00FFFFFF,&H00000000,&H00000000,-1,0,0,0,100,100,1.5,0,1,2,1.7,${orientation},10,10,10,1

[Events]
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text
Comment: 0,0:00:00.00,0:00:00.00,Default,,0,0,0,template line,{\\r\\t($start,$mid,0.5,\\shad6)\\t($mid,$end,0.5,\\shad1.5)}
`;

const buildLine = (start, end, text) =>
  `Dialogue: 0,${start},${end},Default,,0,0,0,,${text}`;

function pad(string) {
  return ("0" + string).slice(-2);
}

const formatTime = centiseconds => {
  const date = new Date(centiseconds * 10);
  const hh = date.getUTCHours();
  const mm = date.getUTCMinutes();
  const ss = pad(date.getUTCSeconds());
  const cs = parseInt(date.getUTCMilliseconds() / 10);

  return `${hh}:${pad(mm)}:${ss}.${pad(cs)}`;
};

export { template, buildLine, formatTime };
