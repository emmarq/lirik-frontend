import React, { Component } from "react";
import Word from "./Word";

class Line extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.node.version !== this.props.node.version;
  }
  render() {
    const words = [];
    let wordNode = this.props.node.mainLeaf;
    while (wordNode) {
      words.push(
        <Word
          key={wordNode.id}
          node={wordNode}
          dispatch={this.props.dispatch}
        />
      );
      wordNode = wordNode.next;
    }
    return <div>{words}</div>;
  }
}

export default Line;
