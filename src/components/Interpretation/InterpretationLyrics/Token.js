import React, { Component } from "react";
import Letter from "./Letter";

class Token extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.node.version !== this.props.node.version;
  }
  render() {
    const letters = [];
    let letterNode = this.props.node.mainLeaf;
    while (letterNode) {
      letters.push(
        <Letter
          key={letterNode.id}
          node={letterNode}
          dispatch={this.props.dispatch}
        />
      );
      letterNode = letterNode.next;
    }
    return (
      <span
        style={{
          position: "relative",
          fontSize: 30,
          whiteSpace: "pre-line",
          border: "1px dotted black",
          fontWeight: this.props.node.focus ? "bolder" : ""
        }}
      >
        {letters}
      </span>
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.node.focus && this.props.node.focus)
      this.props.dispatch({ type: "seekPlayer", time: this.props.node.init });
  }
}

export default Token;
