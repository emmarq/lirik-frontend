import React, { Component } from "react";
import Token from "./Token";

class Word extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.node.version !== this.props.node.version;
  }
  render() {
    const tokens = [];
    let tokenNode = this.props.node.mainLeaf;
    while (tokenNode) {
      tokens.push(
        <Token
          key={tokenNode.id}
          node={tokenNode}
          dispatch={this.props.dispatch}
        />
      );
      tokenNode = tokenNode.next;
    }
    return tokens;
  }
}

export default Word;
