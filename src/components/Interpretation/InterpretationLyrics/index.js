import React from "react";
import { updateInterpretation } from "../Service";
import Lyric from "./Lyric";
import Player from "../../Player";
import styles from "./interpretation-lyrics.module.css";
import _ from "lodash";
import Duration from "../../Player/Duration";
import message from "../../Message/state";
import { fonts, orientations, downloadAssFile } from "./sub_downloader";
import { withTranslation } from "react-i18next";

class InterpretationLyrics extends React.PureComponent {
  state = {
    fetched: false,
    playing: false,
    factor: -0.3,
    subOffset: "0",
    subFont: fonts[5],
    subFontSize: "90",
    subOrientation: orientations[4]
  };
  player = React.createRef();
  inputMedia = React.createRef();
  updateInterpretation = async () => {
    try {
      const updated = await updateInterpretation(this.props.interpretation);
      this.props.setInterpretation(updated);
      this.setState({ ...this.state, change: false });
      message.success(this.props.t("performance_updated"));
      return updated;
    } catch (e) {
      message.error(e);
      return e;
    }
  };
  dispatch = action => {
    if (action.type === "seekPlayer") {
      if (!this.state.playing && !this.state.mark && !isNaN(action.time))
        this.player.current.seekTo(action.time);
      return;
    }
    this.setState(state => ({
      ...state,
      changed: state.changed || action.type !== "focus"
    }));
    this.props.setInterpretation(interpretation => {
      const lyricTree = interpretation.serialized;
      const nextTree = lyricTree.dispatch(action);
      return { ...interpretation, serialized: nextTree };
    });
  };
  changeMedia = () => {
    this.setState({ ...this.state, changed: true });
    this.props.setInterpretation(interpretation => {
      return { ...interpretation, media: this.inputMedia.current.value };
    });
  };
  playPauseClick = () => {
    this.setState({ ...this.state, playing: !this.state.playing });
  };
  checkMark = e => {
    const checked = e.target.checked;
    this.setState(state => ({ ...this.state, mark: checked }));
    this.dispatch({ type: "clearCursor" });
  };
  onProgress = _.throttle(
    data => {
      if (!this.state.mark) {
        const lyricTree = this.props.interpretation.serialized;
        const node = lyricTree.findNodeOnTime(data.playedSeconds);
        if (node) {
          const address = node.getAddress();
          if (
            !lyricTree.cursor ||
            lyricTree.cursor[lyricTree.cursor.length - 2] !==
              address[address.length - 1]
          ) {
            const nextTree = lyricTree.dispatch({
              type: "focus",
              payload: { node: node.mainLeaf }
            });
            this.props.setInterpretation({
              ...this.props.interpretation,
              serialized: nextTree
            });
          }
        }
      }
    },
    100,
    {
      trailing: false
    }
  );
  markInit = () => {
    if (!this.start) {
      this.start = true;
      this.dispatch({
        type: "markInit",
        payload: { time: this.player.current.getCurrentTime() }
      });
    }
  };
  markEnd = () => {
    if (this.start) {
      this.start = false;
      this.dispatch({
        type: "markEnd",
        payload: { time: this.player.current.getCurrentTime() }
      });
    }
  };
  alignStartTimeOfCurrentWithPrevious = () => {
    this.dispatch({ type: "alignStartTimeOfCurrentWithPrevious" });
  };
  alignEndTimeOfCurrentWithNext = () => {
    this.dispatch({ type: "alignEndTimeOfCurrentWithNext" });
  };
  increaseStartTimeOfCurrent = () => {
    this.dispatch({ type: "increaseStartTimeOfCurrent" });
  };
  decreaseStartTimeOfCurrent = () => {
    this.dispatch({ type: "decreaseStartTimeOfCurrent" });
  };
  increaseEndTimeOfCurrent = () => {
    this.dispatch({ type: "increaseEndTimeOfCurrent" });
  };

  decreaseEndTimeOfCurrent = () => {
    this.dispatch({ type: "decreaseEndTimeOfCurrent" });
  };
  changeFactor = e => {
    this.setState({ ...this.state, factor: parseFloat(e.target.value) });
  };
  changeAllTime = () => {
    this.dispatch({
      type: "changeAllTime",
      payload: { factor: this.state.factor }
    });
  };
  clear = e => {
    e.target.value = "";
  };
  focusToken = token => {
    this.dispatch({ type: "focus", payload: { node: token.mainLeaf } });
  };
  karaokeParamChange = e => {
    this.setState({ ...this.state, [e.target.id]: e.target.value });
  };
  downloadAssFile = () => {
    downloadAssFile(
      this.props.interpretation,
      this.state.subOffset,
      this.state.subFontSize,
      this.state.subFont,
      orientations.indexOf(this.state.subOrientation) + 1
    );
  };

  render() {
    const { interpretation } = this.props;

    const saveAction = this.state.changed ? (
      <div
        className="extra"
        style={{ cursor: "pointer" }}
        onClick={this.updateInterpretation}
      >
        <i className="fas fa-save" />
      </div>
    ) : null;

    const markClickEvents = {
      onMouseDown: this.markInit,
      onMouseUp: this.markEnd
    };

    return (
      <div className={`${styles.interpretation}`}>
        <div className={`${styles.lyrics} card`}>
          <div className={`card-title`}>
            {interpretation.name}
            {saveAction}
          </div>
          <div className={`card-body`}>
            <Lyric
              time={this.state.time}
              tree={interpretation.serialized}
              dispatch={this.dispatch}
            />
          </div>
        </div>
        <div className={`${styles.media} card`}>
          <Player
            style={{ height: 250 }}
            url={interpretation.media}
            playing={this.state.playing}
            onProgress={this.onProgress}
            playPauseClick={this.playPauseClick}
            ref={this.player}
          />
          <div className={styles.mediaSource}>
            <input
              ref={this.inputMedia}
              placeholder="media url"
              type="text"
              defaultValue={interpretation.media}
            />
            <button onClick={this.changeMedia}>
              <i className="fas fa-video" />
            </button>
          </div>
        </div>
        <div className={`${styles.spyglass} card`}>
          <div className={`body ${styles.tokenPanel}`}>
            <div className={styles.tokenPreviews}>
              <TokenPreviews
                tree={interpretation.serialized}
                focus={this.focusToken}
              />
            </div>
            <div className={styles.tokenDashboard}>
              <button
                onClick={this.alignStartTimeOfCurrentWithPrevious}
                title={this.props.t("align_start_time")}
              >
                <i className="fas fa-fast-backward" />
              </button>
              <button
                onClick={this.alignEndTimeOfCurrentWithNext}
                title={this.props.t("align_end_time")}
              >
                <i className="fas fa-fast-forward" />
              </button>
              <button
                onClick={this.increaseStartTimeOfCurrent}
                title={this.props.t("increase_start_time")}
              >
                <i className="fas fa-chevron-up" />
              </button>
              <button
                onClick={this.decreaseStartTimeOfCurrent}
                title={this.props.t("decrease_start_time")}
              >
                <i className="fas fa-chevron-down" />
              </button>
              <button
                onClick={this.increaseEndTimeOfCurrent}
                title={this.props.t("increase_end_time")}
              >
                <i className="fas fa-caret-up" />
              </button>
              <button
                onClick={this.decreaseEndTimeOfCurrent}
                title={this.props.t("decrease_end_time")}
              >
                <i className="fas fa-caret-down" />
              </button>
              <div style={{ display: "inline" }}>
                <input
                  style={{ width: 40 }}
                  type="number"
                  value={this.state.factor}
                  step={0.1}
                  onChange={this.changeFactor}
                />
                <button
                  onClick={this.changeAllTime}
                  title={this.props.t("change_all_times")}
                >
                  <i className="fas fa-arrows-alt-v" />
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className={`${styles.sync} card`}>
          <div className="body">
            <p>
              <input
                id="mark_id"
                type="checkbox"
                onChange={this.checkMark}
                checked={this.state.mark}
              />{" "}
              <label htmlFor="mark_id">{this.props.t("enable_mark")}</label>
            </p>
            <div style={{ display: "flex" }}>
              <button
                disabled={!this.state.mark}
                {...markClickEvents}
                style={{ flexGrow: 1, padding: "20px 10px" }}
              >
                {this.props.t("mark")}
              </button>
              <button
                disabled={!this.state.mark}
                {...markClickEvents}
                style={{ flexGrow: 1, padding: "20px 10px" }}
              >
                {this.props.t("mark")}
              </button>
            </div>
            <textarea
              disabled={!this.state.mark}
              placeholder={this.props.t("keyboard_mark")}
              style={{ width: "100%", resize: "none" }}
              onKeyDown={this.markInit}
              onKeyUp={this.markEnd}
              onChange={this.clear}
            />
          </div>
        </div>
        <div className={`${styles.karaoke} card`}>
          <div className="body">
            <div>
              <label>{this.props.t("font_type")}</label>
              <select
                id="subFont"
                value={this.state.subFont}
                onChange={this.karaokeParamChange}
              >
                {fonts.map(font => (
                  <option key={font} value={font}>
                    {font}
                  </option>
                ))}
              </select>
            </div>
            <div>
              <label>{this.props.t("font_size")}</label>
              <input
                type="number"
                id="subFontSize"
                value={this.state.subFontSize}
                onChange={this.karaokeParamChange}
              />
            </div>
            <div>
              <label>{this.props.t("lyric_orientation")}</label>
              <select
                id="subOrientation"
                value={this.state.subOrientation}
                onChange={this.karaokeParamChange}
              >
                {orientations.map(orientation => (
                  <option key={orientation} value={orientation}>
                    {orientation}
                  </option>
                ))}
              </select>
            </div>
            <div>
              <label>{this.props.t("karaoke_offset")}</label>
              <input
                type="number"
                id="subOffset"
                value={this.state.subOffset}
                onChange={this.karaokeParamChange}
              />
            </div>
            <button onClick={this.downloadAssFile}>
              {this.props.t("download_subtitle")}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const TokenPreviews = ({ tree, focus }) => {
  const tokens = [];
  if (tree.hasCursor()) {
    let prev = 4;
    let next = 4;
    const node = tree.findNode(tree.cursor);
    if (node) {
      const currentToken = node.nodeRoot;

      let prevToken = currentToken.getPrev();
      let nextToken = currentToken.getNext();
      while (prevToken && prev > 0) {
        tokens.splice(
          0,
          0,
          <TokenPreview key={prevToken.id} token={prevToken} focus={focus} />
        );
        prevToken = prevToken.getPrev();
        prev--;
      }
      tokens.push(
        <TokenPreview
          key={currentToken.id}
          style={{ fontWeight: "bold", fontSize: "1.2em" }}
          token={currentToken}
        />
      );
      while (nextToken && next > 0) {
        tokens.push(
          <TokenPreview key={nextToken.id} token={nextToken} focus={focus} />
        );
        nextToken = nextToken.getNext();
        next--;
      }
    }
  }
  return tokens;
};

class TokenPreview extends React.Component {
  focus = () => {
    this.props.focus && this.props.focus(this.props.token);
  };
  render() {
    const { token, style } = this.props;
    let letters = "";
    let letterNode = token.mainLeaf;
    while (letterNode) {
      letters += letterNode.value;
      letterNode = letterNode.next;
    }
    return (
      <div
        style={{ ...style, cursor: "pointer" }}
        className={styles.tokenPreview}
        onClick={this.focus}
      >
        <div className={styles.tokenTime}>
          <Duration seconds={token.init} />
        </div>
        <div className={styles.tokenLetters}>{letters}</div>
        <div className={styles.tokenTime}>
          <Duration seconds={token.end} />
        </div>
      </div>
    );
  }
}

export default withTranslation()(InterpretationLyrics);
