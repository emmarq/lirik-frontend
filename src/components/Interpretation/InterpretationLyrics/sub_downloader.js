import { template, formatTime, buildLine } from "./ass_template";
import { download } from "../../../util/download";

export const orientations = [
  "inferior-izquierdo",
  "inferior-centro",
  "inferior-derecho",
  "centro-izqierdo",
  "centro-centro",
  "centro-derecho",
  "superior-izquierdo",
  "superior-centro",
  "superior-derecho"
];
export const fonts = [
  "Chilanka",
  "Purisa",
  "Sawasdee",
  "Rachana",
  "Dyuthi",
  "Nimbus Sans L",
  "Pagul",
  "Serif",
  "Liberation Serif",
  "Ubuntu"
];

export const downloadAssFile = (
  interpretation,
  ahead,
  size,
  font,
  orientation
) => {
  const lyric = interpretation.serialized;
  const lineOffset = 100; //1 segundo
  const offset = parseInt(ahead);
  const firstLetter = lyric.firstLetter();
  let token = firstLetter && firstLetter.nodeRoot;
  const lines = [];

  lines.push({
    start: "0:00:00.00",
    end: formatTime(token.init * 100 - lineOffset + offset),
    text: (interpretation.name || "LIRIK SONG").replace("\n", "\\N")
  });
  let line = 0;
  while (token) {
    const prevToken = token.getPrev();
    if (isNewParagraph(token)) {
      let start, end;

      const thisInit = token.init * 100;

      if (prevToken) {
        const prevEnd = prevToken.end * 100;
        if (thisInit - prevEnd > 500) {
          start = formatTime(thisInit - lineOffset + offset);
          end = formatTime(prevEnd + lineOffset);
        } else {
          start = formatTime(thisInit - (thisInit - prevEnd) / 2 + offset);
          end = start;
        }
      } else {
        start = formatTime(thisInit - lineOffset + offset);
      }

      if (line > 0) lines[line].end = end;
      lines.push({ start: start, end: 0, text: "" });
      line++;
    }
    lines[line].text += tag(token, prevToken);
    const nextToken = token.getNext();
    if (!nextToken) lines[line].end = formatTime(token.end * 100 + 100);
    token = nextToken;
  }
  let sub = template(font, size, orientation);
  lines.forEach(l => {
    sub += buildLine(l.start, l.end, l.text) + "\n";
  });
  const filename = interpretation.name;
  download(filename + ".ass", sub);
};

const isNewParagraph = token => {
  if (token.isMainLeaf()) {
    const word = token.nodeRoot;
    if (word.isMainLeaf()) {
      const line = word.nodeRoot;
      return line.isMainLeaf();
    }
  }
  return false;
};

const tag = (token, prevToken) => {
  let sincePrevToken = parseInt(
    (token.init - (prevToken ? prevToken.end : token.init - 1)) * 100
  );

  if (isNewParagraph(token)) {
    if (sincePrevToken < 500) {
      sincePrevToken = parseInt(sincePrevToken / 2);
    } else {
      sincePrevToken = 100;
    }
  }
  const selfDuration = parseInt((token.end - token.init) * 100);

  let text = "";
  let letter = token.mainLeaf;
  while (letter) {
    text += letter.value;
    letter = letter.next;
  }
  if (!token.next && !token.nodeRoot.next) text += "\\N"; //the last token of the last word of the line

  return `{\\k${sincePrevToken}}{\\k${selfDuration}}${text}`;
};
//{\r\t($start,$mid,0.5,\shad6)\t($mid,$end,0.5,\shad1.5)}
