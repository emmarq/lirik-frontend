import React, { Component } from "react";
import Paragraph from "./Paragraph";

class Lyric extends Component {
  state = {};
  inputDelegate = React.createRef();
  handleSpecialKeys = e => {
    if (e.which === 32) {
      this.props.dispatch({ type: "handleSpaceKey" });
      e.preventDefault();
    }
    if (e.which === 13) {
      this.props.dispatch({ type: "handleEnterKey" });
      e.preventDefault();
    }
    if (e.which === 8) {
      this.props.dispatch({ type: "handleBackspaceKey" });
      e.preventDefault();
    }
    if (e.which === 46) {
      this.props.dispatch({ type: "handleDeleteKey" });
      e.preventDefault();
    }
    if (e.which === 37) {
      this.props.dispatch({ type: "handleLeftKey" });

      e.preventDefault();
    }
    if (e.which === 39) {
      this.props.dispatch({ type: "handleRightKey" });
      e.preventDefault();
    }
  };
  addText = e => {
    this.props.dispatch({ type: "addText", payload: { text: e.target.value } });
    e.target.value = "";
  };
  render() {
    const paragraphs = [];
    let paragraphNode = this.props.tree.root.mainLeaf;
    let textAreaStyle = {
      height: 0,
      width: 0,
      margin: 0,
      padding: 0,
      border: 0,
      resize: "none",
      display: "inline",
      position: "fixed"
    };
    if (!paragraphNode) {
      textAreaStyle = {
        fontSize: 20,
        height: 100,
        margin: 0,
        padding: 0,
        border: 0,
        resize: "none",
        outline: "none"
      };
    }
    while (paragraphNode) {
      paragraphs.push(
        <Paragraph
          key={paragraphNode.id}
          node={paragraphNode}
          dispatch={this.props.dispatch}
        />
      );
      paragraphNode = paragraphNode.next;
    }

    return (
      <div>
        <textarea
          id="input-text"
          ref={this.inputDelegate}
          style={textAreaStyle}
          onChange={this.addText}
          onKeyDown={this.handleSpecialKeys}
          onFocus={this.avoidScroll}
          placeholder="Copie aquí la letra de la interpretación"
        />
        {paragraphs}
      </div>
    );
  }
  avoidScroll = e => {
    e.preventDefault();
    e.target.focus({ preventScroll: true });
  };
}

export default Lyric;
