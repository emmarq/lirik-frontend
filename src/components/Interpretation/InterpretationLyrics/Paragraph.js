import React, { Component } from "react";
import Line from "./Line";

class Paragraph extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.node.version !== this.props.node.version;
  }
  render() {
    const lines = [];
    let lineNode = this.props.node.mainLeaf;
    while (lineNode) {
      lines.push(
        <Line
          key={lineNode.id}
          node={lineNode}
          dispatch={this.props.dispatch}
        />
      );
      lineNode = lineNode.next;
    }
    return (
      <div>
        {lines}
        <br />
        <br />
      </div>
    );
  }
}

export default Paragraph;
