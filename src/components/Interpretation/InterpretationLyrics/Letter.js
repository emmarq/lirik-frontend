import React, { Component } from "react";

const bottomButtonStyle = {
  cursor: "pointer",
  lineHeight: "16px",
  fontSize: 30,
  textAlign: "center",
  userSelect: "none",
  left: 0,
  right: 0,
  position: "absolute"
};

class Letter extends Component {
  state = {};
  focus = () => {
    this.props.dispatch({
      type: "focus",
      payload: { node: this.props.node }
    });
  };
  moveRight = () => {
    this.props.dispatch({
      type: "moveRight",
      payload: { node: this.props.node }
    });
  };
  moveLeft = () => {
    this.props.dispatch({
      type: "moveLeft",
      payload: { node: this.props.node }
    });
  };
  split = () => {
    this.props.dispatch({
      type: "split",
      payload: { node: this.props.node }
    });
  };
  onMouseDown = e => {
    this.initialX = e.screenX;
    this.setState({ ...this.state, grabbed: true });
  };
  onMouseMove = e => {
    if (this.state.grabbed) {
      this.orientation = e.screenX > this.initialX ? "right" : "left";
    }
  };
  onMouseLeave = e => {
    if (this.state.grabbed && this.orientation) {
      if (this.orientation === "left") this.moveLeft();
      else this.moveRight();
      this.setState({ ...this.state, grabbed: false });
      this.orientation = null;
    }
  };
  onTouchStart = e => {
    this.initialX = e.touches[0].clientX;
    this.setState({ ...this.state, grabbed: true });
  };
  onTouchMove = e => {
    if (this.state.grabbed) {
      this.orientation =
        e.changedTouches[0].clientX > this.initialX ? "right" : "left";
    }
  };
  onTouchEnd = e => {
    if (this.state.grabbed) {
      this.setState({ ...this.state, grabbed: false });
      this.props.onMoveLetter(this.props.letter.id, this.orientation);
    }
  };
  mouseEvents = {
    onMouseDown: this.onMouseDown,
    onMouseMove: this.onMouseMove,
    onMouseLeave: this.onMouseLeave,
    onMouseUp: this.onMouseLeave,
    onTouchStart: this.onTouchStart,
    onTouchMove: this.onTouchMove,
    onTouchEnd: this.onTouchEnd
  };
  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.state.grabbed !== nextState.grabbed ||
      nextProps.node.version !== this.props.node.version
    );
  }
  render() {
    const node = this.props.node;
    const first = !node.prev;
    const last = !node.next;

    const topButton = !first ? (
      <div
        onClick={this.split}
        style={{
          cursor: "pointer",
          lineHeight: "16px",
          fontSize: 30,
          textAlign: "center",
          color: "red",
          margin: "0px 2px"
        }}
      >
        &#8226;
      </div>
    ) : null;

    let bottomButton = null;

    if (first && last) {
      bottomButton = (
        <div
          {...this.mouseEvents}
          style={{
            ...bottomButtonStyle,
            cursor: this.state.grabbed ? "grabbing" : "pointer"
          }}
        >
          &#8226;
        </div>
      );
    } else {
      if (first) {
        bottomButton = (
          <div
            onClick={this.moveLeft}
            style={{ ...bottomButtonStyle, fontSize: 15 }}
          >
            <i className="fas fa-caret-left" />
          </div>
        );
      }
      if (last) {
        bottomButton = (
          <div
            onClick={this.moveRight}
            style={{ ...bottomButtonStyle, fontSize: 15 }}
          >
            <i className="fas fa-caret-right" />
          </div>
        );
      }
    }

    return (
      <div
        style={{
          display: "inline-block",
          position: "relative",
          marginBottom: 40,
          zIndex: 1
        }}
      >
        {topButton}
        <label
          style={{
            textAlign: "center",
            cursor: "text",
            display: "block",
            borderLeft: node.focus ? "1px solid red" : ""
          }}
          htmlFor="input-text"
          onClick={this.focus}
        >
          {node.value}
        </label>
        {bottomButton}
      </div>
    );
  }
}

export default Letter;
