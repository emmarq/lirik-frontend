import React, { Component } from "react";
import InterpretationForm from "../InterpretationForm";
import styles from "./edit-interpretation.module.css";
import { createInterpretation, updateInterpretation } from "../Service";
import message from "../../Message/state";
import { withTranslation } from "react-i18next";

class EditInterpretation extends Component {
  editInterpretation = interpretation => {
    const interpretationPromise = interpretation.id
      ? updateInterpretation
      : createInterpretation;
    interpretationPromise(interpretation)
      .then(updated => {
        message.success(
          this.props.t(
            interpretation.id ? "performance_updated" : "performance_created"
          )
        );
        this.props.setInterpretation(updated);
        this.props.history.push(`/interpretations/${updated.id}`);
      })
      .catch(e => {
        message.error(e);
      });
  };
  cancel = () => {
    this.props.history.push(`/interpretations/${this.props.match.params.id}`);
  };
  render() {
    return (
      <div>
        <div className={`${styles.editInterpretation} card`}>
          <div className="card-body">
            <InterpretationForm
              data={this.props.interpretation}
              submit={this.editInterpretation}
              setInterpretation={this.props.setInterpretation}
              cancel={this.cancel}
              submitLabel={this.props.t(
                this.props.interpretation.id
                  ? "update_interpretation"
                  : "create_interpretation"
              )}
              cancelLabel={this.props.t("cancel")}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(EditInterpretation);
