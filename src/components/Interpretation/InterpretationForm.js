import React, { Component } from "react";
import ArtistPicker from "../Artist/ArtistPicker";
import BandPicker from "../Band/BandPicker";
import AlbumPicker from "../Album/AlbumPicker";
import SongPicker from "../Song/SongPicker";
import GenrePicker from "../Genre/GenrePicker";
import { FormModel, notEmpty, digitsOnly } from "../../util/FormModel";
import { Modal, OptionSelector } from "../../util/components";
import styles from "./interpretation_form.module.css";
import { withTranslation } from "react-i18next";

class InterpretationForm extends Component {
  constructor(props) {
    super(props);
    this.model = new FormModel(this);
    this.model.addValidation("name", notEmpty, props.t("which_name"));
    this.model.addValidation("description", notEmpty, props.t("how_so"));
    this.model.addValidation("year", notEmpty, props.t("which_year"));
    this.model.addValidation("year", digitsOnly, props.t("year_number"));
    this.model.addValidation("lang", notEmpty, props.t("which_lang"));
    this.model.addValidation("song_id", notEmpty, props.t("which_song"));
    this.performerTypes = [
      { value: "Artist", label: props.t("artist") },
      { value: "Band", label: props.t("band") }
    ];
    this.languages = [
      { value: "english", label: props.t("en") },
      { value: "spanish", label: props.t("es") }
    ];
    this.state = {
      data: {
        name: "",
        description: "",
        year: "",
        lang: "",
        interpretation_performers: [],
        interpretation_genres: [],
        album_interpretations: [],
        song: {}
      },
      errors: {},
      performerType: this.performerTypes[0]
    };
  }
  showSongPicker = () => {
    this.setState(state => ({ ...state, showSongPicker: true }));
  };
  hideSongPicker = () => {
    this.setState(state => ({ ...state, showSongPicker: false }));
  };
  pickSong = song => {
    this.hideSongPicker();
    this.props.setInterpretation({
      ...this.state.data,
      song_id: song.id,
      song: song
    });
  };
  showPerformerPicker = () => {
    this.setState(state => ({ ...state, showPerformerPicker: true }));
  };
  hidePerformerPicker = () => {
    this.setState(state => ({ ...state, showPerformerPicker: false }));
  };
  pickPerformer = performer => {
    this.hidePerformerPicker();
    for (let ip of this.state.data.interpretation_performers) {
      if (
        ip.performer_id === performer.id &&
        ip.performer_type === this.state.performerType.value
      )
        return;
    }
    this.props.setInterpretation({
      ...this.state.data,
      interpretation_performers: [
        ...this.state.data.interpretation_performers,
        {
          performer: performer,
          performer_id: performer.id,
          performer_type: this.state.performerType.value
        }
      ]
    });
  };
  changePerformerType = performerType => {
    this.setState(state => ({ ...state, performerType: performerType }));
  };
  togglePerformer = index => {
    const nextState = { ...this.state.data };
    if (nextState.interpretation_performers[index].id)
      nextState.interpretation_performers[index]._destroy = !nextState
        .interpretation_performers[index]._destroy;
    else nextState.interpretation_performers.splice(index, 1);
    this.props.setInterpretation(nextState);
  };
  showAlbumPicker = () => {
    this.setState(state => ({ ...state, showAlbumPicker: true }));
  };
  hideAlbumPicker = () => {
    this.setState(state => ({ ...state, showAlbumPicker: false }));
  };
  pickAlbum = album => {
    this.hideAlbumPicker();
    for (let ai of this.state.data.album_interpretations) {
      if (ai.album_id === album.id) return;
    }
    this.props.setInterpretation({
      ...this.state.data,
      album_interpretations: [
        ...this.state.data.album_interpretations,
        { album: album, album_id: album.id }
      ]
    });
  };
  toggleAlbum = index => {
    const nextState = { ...this.state.data };
    if (nextState.album_interpretations[index].id)
      nextState.album_interpretations[index]._destroy = !nextState
        .album_interpretations[index]._destroy;
    else nextState.album_interpretations.splice(index, 1);
    this.props.setInterpretation(nextState);
  };
  showGenrePicker = () => {
    this.setState(state => ({ ...state, showGenrePicker: true }));
  };
  hideGenrePicker = () => {
    this.setState(state => ({ ...state, showGenrePicker: false }));
  };
  pickGenre = genre => {
    this.hideGenrePicker();
    for (let ig of this.state.data.interpretation_genres) {
      if (ig.genre_id === genre.id) return;
    }
    this.props.setInterpretation({
      ...this.state.data,
      interpretation_genres: [
        ...this.state.data.interpretation_genres,
        { genre: genre, genre_id: genre.id }
      ]
    });
  };
  toggleGenre = index => {
    const nextState = { ...this.state.data };
    if (nextState.interpretation_genres[index].id)
      nextState.interpretation_genres[index]._destroy = !nextState
        .interpretation_genres[index]._destroy;
    else nextState.interpretation_genres.splice(index, 1);
    this.props.setInterpretation(nextState);
  };
  onChange = e => {
    this.props.setInterpretation({
      ...this.state.data,
      [e.target.id]: e.target.value
    });
  };
  submit = e => {
    e.preventDefault();
    if (this.model.isValid()) {
      const interpretation = { ...this.model.getData() };
      const interpretation_genres = [];
      const album_interpretations = [];
      const interpretation_performers = [];

      for (let ai of interpretation.album_interpretations) {
        const album_interpretation = { ...ai };
        delete album_interpretations.album;
        delete album_interpretations.interpretation;
        album_interpretations.push(album_interpretation);
      }

      for (let ig of interpretation.interpretation_genres) {
        const interpretation_genre = { ...ig };
        delete interpretation_genre.genre;
        delete interpretation_genre.interpretation;
        interpretation_genres.push(interpretation_genre);
      }

      for (let ip of interpretation.interpretation_performers) {
        const interpretation_performer = { ...ip };
        delete interpretation_performer.performer;
        delete interpretation_performer.interpretation;
        interpretation_performers.push(interpretation_performer);
      }

      interpretation.album_interpretations_attributes = album_interpretations;
      interpretation.interpretation_genres_attributes = interpretation_genres;
      interpretation.interpretation_performers_attributes = interpretation_performers;

      delete interpretation.album_interpretations;
      delete interpretation.interpretation_performers;
      delete interpretation.interpretation_genres;
      delete interpretation.song;

      this.props.submit(interpretation);
    }
  };
  cancel = () => {
    this.props.cancel();
  };
  init = props => {
    this.setState(state => ({
      ...state,
      data: { ...state.data, ...(props.data || {}) }
    }));
  };
  componentDidMount = () => {
    this.init(this.props);
  };
  componentWillReceiveProps = nextProps => {
    this.init(nextProps);
  };
  render = () => {
    return (
      <div className="interpretation-form">
        <div className="generic-input">
          <label htmlFor="">
            {this.props.t("song")}
            <button
              className={styles.addPerformer}
              onClick={this.showSongPicker}
            >
              {this.props.t("pick_song")}
            </button>
          </label>
          {this.state.data.song.name}
          {this.model.getErrors("song_id")}
          <Modal
            show={this.state.showSongPicker}
            close={this.hideSongPicker}
            title={
              <div>
                {this.props.t("song_selection")}
                <button
                  style={{ float: "right" }}
                  onClick={this.hideSongPicker}
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            }
          >
            <SongPicker pick={this.pickSong} cancel={this.hideSongPicker} />
          </Modal>
        </div>

        <div className="generic-input">
          <label htmlFor="name">{this.props.t("name")}</label>
          <input
            id="name"
            type="text"
            value={this.model.getField("name")}
            onChange={this.onChange}
            autoComplete="true"
          />
          {this.model.getErrors("name")}
        </div>
        <div className="generic-input">
          <label htmlFor="description">{this.props.t("description")}</label>
          <input
            id="description"
            type="text"
            value={this.model.getField("description")}
            onChange={this.onChange}
            autoComplete="true"
          />
          {this.model.getErrors("description")}
        </div>
        <div className="generic-input">
          <label htmlFor="year">{this.props.t("year")}</label>
          <input
            id="year"
            type="text"
            value={this.model.getField("year")}
            onChange={this.onChange}
            autoComplete="true"
          />
          {this.model.getErrors("year")}
        </div>
        <div className="generic-input">
          <label htmlFor="lang">{this.props.t("language")}</label>
          <select
            id="lang"
            value={this.model.getField("lang")}
            onChange={this.onChange}
          >
            {this.languages.map(lang => (
              <option key={lang.value} value={lang.value}>
                {lang.label}
              </option>
            ))}
          </select>
          {this.model.getErrors("lang")}
        </div>
        <div className="generic-input">
          <label htmlFor="">
            {this.props.t("performers")}
            <button
              className={styles.addPerformer}
              onClick={this.showPerformerPicker}
            >
              {this.props.t("add_performer")}
            </button>
          </label>
          <ul className={styles.performers}>
            {this.state.data.interpretation_performers.map(
              (interpretation_performer, i) => {
                return (
                  <Performer
                    key={interpretation_performer.id}
                    id={i}
                    toggle={this.togglePerformer}
                    interpretationPerformer={interpretation_performer}
                  />
                );
              }
            )}
          </ul>
          <Modal
            show={this.state.showPerformerPicker}
            close={this.hidePerformerPicker}
            title={
              <div>
                {this.props.t("selection_of")}
                <OptionSelector
                  value={this.state.performerType}
                  options={this.performerTypes}
                  select={this.changePerformerType}
                />
                <button
                  style={{ float: "right" }}
                  onClick={this.hidePerformerPicker}
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            }
          >
            {this.state.performerType.value === "Artist" && (
              <ArtistPicker
                pick={this.pickPerformer}
                cancel={this.hidePerformerPicker}
              />
            )}
            {this.state.performerType.value === "Band" && (
              <BandPicker
                pick={this.pickPerformer}
                cancel={this.hidePerformerPicker}
              />
            )}
          </Modal>
        </div>

        <div className="generic-input">
          <label htmlFor="">
            {this.props.t("albums")}

            <button
              className={styles.addPerformer}
              onClick={this.showAlbumPicker}
            >
              {this.props.t("add_album")}
            </button>
          </label>
          <ul className={styles.albums}>
            {this.state.data.album_interpretations.map(
              (album_interpretation, i) => {
                return (
                  <Album
                    key={album_interpretation.id}
                    id={i}
                    toggle={this.toggleAlbum}
                    albumInterpretation={album_interpretation}
                  />
                );
              }
            )}
          </ul>
          <Modal
            show={this.state.showAlbumPicker}
            close={this.hideAlbumPicker}
            title={
              <div>
                {this.props.t("album_selection")}
                <button
                  style={{ float: "right" }}
                  onClick={this.hideAlbumPicker}
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            }
          >
            <AlbumPicker pick={this.pickAlbum} cancel={this.hideAlbumPicker} />
          </Modal>
        </div>
        <div className="generic-input">
          <label htmlFor="">
            {this.props.t("genres")}
            <button
              className={styles.addPerformer}
              onClick={this.showGenrePicker}
            >
              {this.props.t("add_genre")}
            </button>
          </label>
          <ul className={styles.genres}>
            {this.state.data.interpretation_genres.map(
              (interpretation_genre, i) => {
                return (
                  <Genre
                    key={interpretation_genre.id}
                    id={i}
                    toggle={this.toggleGenre}
                    interpretationGenre={interpretation_genre}
                  />
                );
              }
            )}
          </ul>
          <Modal
            show={this.state.showGenrePicker}
            close={this.hideGenrePicker}
            title={
              <div>
                {this.props.t("genre_selection")}
                <button
                  style={{ float: "right" }}
                  onClick={this.hideGenrePicker}
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            }
          >
            <GenrePicker pick={this.pickGenre} cancel={this.hideGenrePicker} />
          </Modal>
        </div>
        <div>
          <button onClick={this.submit}>{this.props.submitLabel}</button>
          <button onClick={this.cancel}>{this.props.cancelLabel}</button>
        </div>
      </div>
    );
  };
}

class Performer extends Component {
  toggle = () => {
    this.props.toggle(this.props.id);
  };
  render = () => {
    const { interpretationPerformer } = this.props;
    if (!interpretationPerformer.performer) return null;
    return (
      <li onClick={this.toggle}>
        <span
          style={
            interpretationPerformer._destroy
              ? { textDecoration: "line-through" }
              : {}
          }
        >
          {interpretationPerformer.performer.artistic_name}
        </span>
        <i
          style={{ marginLeft: 5 }}
          className={
            interpretationPerformer._destroy ? "fas fa-times" : "fas fa-check"
          }
        />
      </li>
    );
  };
}

class Album extends Component {
  toggle = () => {
    this.props.toggle(this.props.id);
  };
  render = () => {
    const { albumInterpretation } = this.props;
    if (!albumInterpretation.album) return null;
    return (
      <li onClick={this.toggle}>
        <span
          style={
            albumInterpretation._destroy
              ? { textDecoration: "line-through" }
              : {}
          }
        >
          {albumInterpretation.album.name}
        </span>
        <i
          style={{ marginLeft: 5 }}
          className={
            albumInterpretation._destroy ? "fas fa-times" : "fas fa-check"
          }
        />
      </li>
    );
  };
}

class Genre extends Component {
  toggle = () => {
    this.props.toggle(this.props.id);
  };
  render = () => {
    const { interpretationGenre } = this.props;
    if (!interpretationGenre.genre) return null;
    return (
      <li onClick={this.toggle}>
        <span
          style={
            interpretationGenre._destroy
              ? { textDecoration: "line-through" }
              : {}
          }
        >
          {interpretationGenre.genre.name}
        </span>
        <i
          style={{ marginLeft: 5 }}
          className={
            interpretationGenre._destroy ? "fas fa-times" : "fas fa-check"
          }
        />
      </li>
    );
  };
}

export default withTranslation()(InterpretationForm);
