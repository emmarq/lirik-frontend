import React, { Component } from "react";
import { Link } from "react-router-dom";
import queryString from "query-string";
import styles from "./songs.module.css";
import { getSongs } from "../Service";

class Songs extends Component {
  langs = [
    { id: "spanish", name: "Español" },
    { id: "english", name: "Inglés" }
  ];
  state = { params: {}, page: {}, fetched: false };
  handlePageClick = data => {
    console.log(data);
  };
  onClick = e => {};
  onChange = e => {
    this.setState({
      ...this.state,
      params: { ...this.state.params, [e.target.id]: e.target.value }
    });
    clearTimeout(this.request);
    this.request = setTimeout(() => {
      this.state.params.search &&
        this.requestSongs(queryString.stringify(this.state.params));
    }, 400);
  };
  requestSongs = queryPart => {
    getSongs(queryPart)
      .then(page => {
        this.setState({ ...this.state, fetched: true, page: page });
      })
      .catch(e => {
        this.setState({ ...this.state, fetched: true });
      });
  };
  componentDidMount() {
    this.requestSongs(this.props.location.search.slice(1));
  }
  render() {
    let element = "wait";
    if (this.state.fetched) {
      if (!!this.state.page.total_entries) {
        element = (
          <ul className={styles.songList}>
            {this.state.page.entries.map(song => (
              <li className={styles.songItem} key={song.id}>
                <h3>
                  <Link className={styles.songName} to={`/songs/${song.id}`}>
                    {song.name}
                  </Link>
                </h3>
                <p>
                  {song.artist} <br /> {song.description} {song.year}
                </p>
              </li>
            ))}
          </ul>
        );
      } else {
        element = (
          <div>
            <h2>No hay canciones :(</h2>
            <p>
              <Link to="/songs/new">¡Crea una tu mismo! XD</Link>
            </p>
          </div>
        );
      }
    }
    return (
      <div className={`${styles.songsPage} container card`}>
        <div className="card-title">
          <h2>Canciones</h2>
        </div>
        <div className="card-body">
          <div className={styles.songsBar}>
            <div className={styles.songSearch}>
              <i className="fa fa-search" />
              <input
                id="search"
                type="text"
                placeholder="Busca una canción"
                value={this.state.params.search}
                onChange={this.onChange}
              />
            </div>
            <select
              id="lang"
              value={this.state.params.lang}
              onChange={this.onChange}
            >
              <option value="simple">Seleccione un idioma</option>
              {this.langs.map(lang => (
                <option key={lang.id} value={lang.id}>
                  {lang.name}
                </option>
              ))}
            </select>
            <button>
              <Link to="/songs/new">Nueva canción</Link>
            </button>
          </div>
          {element}
        </div>
      </div>
    );
  }
}

export default Songs;
