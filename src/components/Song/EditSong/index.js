import React, { Component } from "react";
import SongForm from "../SongForm";
import { getSong, updateSong } from "../Service";
import styles from "./edit-song.module.css";
import message from "../../Message/state";

class NewSong extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.editSong = this.editSong.bind(this);
    this.cancel = this.cancel.bind(this);
  }
  editSong(song) {
    updateSong(song)
      .then(updated => {
        message.success("Canción actualizada");
        //this.props.history.push(`/songs/${updated.id}`);
      })
      .catch(e => {
        message.error(e);
      });
  }
  cancel() {
    this.props.history.push(`/songs/${this.props.match.params.id}`);
  }
  componentDidMount() {
    getSong(this.props.match.params.id).then(song => {
      this.setState({ song: song });
    });
  }
  render() {
    return !this.state.song ? (
      <div>Espere</div>
    ) : (
      <div className={`${styles.editSong} container card`}>
        <div className="card-title">
          <h2>{this.state.song.name}</h2>
        </div>
        <div className="card-body">
          <SongForm
            header="Información de la canción"
            data={this.state.song}
            submit={this.editSong}
            cancel={this.cancel}
            submitLabel="Actualizar interpretación"
            cancelLabel="Cancelar"
          />
        </div>
      </div>
    );
  }
}

export default NewSong;
