import React, { Component } from "react";
import ArtistPicker from "../Artist/ArtistPicker";
import BandPicker from "../Band/BandPicker";
import { FormModel, notEmpty } from "../../util/FormModel";
import { OptionSelector, Modal } from "../../util/components";
import styles from "./song_form.module.css";

const composerTypes = [
  { value: "Artist", label: "Artista" },
  { value: "Band", label: "Banda" }
];

class SongForm extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.submit = this.submit.bind(this);
    this.cancel = this.cancel.bind(this);
    this.model = new FormModel(this);
    this.model.addValidation("name", notEmpty, "Cual es el nombre");
    this.model.addValidation("year", notEmpty, "En que año");
  }
  state = {
    data: { song_composers: [] },
    errors: {},
    composerType: composerTypes[0]
  };
  showPicker = () => {
    this.setState(state => ({ ...state, showPicker: true }));
  };
  hidePicker = () => {
    this.setState(state => ({ ...state, showPicker: false }));
  };
  pickComposer = composer => {
    this.setState(state => {
      const nextState = { ...this.state };
      nextState.showPicker = false;
      for (let sc of nextState.data.song_composers) {
        if (
          sc.composer_id === composer.id &&
          sc.composer_type === state.composerType.value
        )
          return nextState;
      }
      nextState.data.song_composers = [
        ...nextState.data.song_composers,
        {
          composer: composer,
          composer_id: composer.id,
          composer_type: state.composerType.value
        }
      ];
      return nextState;
    });
  };
  changeComposerType = composerType => {
    this.setState(state => ({ ...state, composerType: composerType }));
  };
  toggleComposer = index => {
    this.setState(state => {
      const nextState = { ...state };
      if (nextState.data.song_composers[index].id)
        nextState.data.song_composers[index]._destroy = !nextState.data
          .song_composers[index]._destroy;
      else nextState.data.song_composers.splice(index, 1);
      return nextState;
    });
  };
  cancelDefaultSubmit = e => {
    e.preventDefault();
  };
  onChange(e) {
    this.model.setField(e.target.id, e.target.value);
  }
  submit(e) {
    e.preventDefault();
    if (this.model.isValid()) {
      const song = { ...this.model.getData() };
      const song_composers = [];
      for (let sc of song.song_composers) {
        const song_composer = { ...sc };
        delete song_composer.composer;
        delete song_composer.song;
        song_composers.push(song_composer);
      }
      song.song_composers_attributes = song_composers;
      delete song.song_composers;
      delete song.interpretations;
      this.props.submit(song);
    }
  }
  cancel() {
    this.props.cancel();
  }
  init(props) {
    this.setState({
      ...this.state,
      data: { ...this.state.data, ...(props.data || {}) }
    });
  }
  componentDidMount() {
    this.init(this.props);
  }
  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }
  render() {
    return (
      <form className="song-form" onSubmit={this.cancelDefaultSubmit}>
        <div className="generic-input">
          <label htmlFor="name">Nombre</label>
          <input
            id="name"
            name="name"
            type="text"
            value={this.model.getField("name")}
            onChange={this.onChange}
            autoComplete="true"
          />
          {this.model.getErrors("name")}
        </div>
        <div className="generic-input">
          <label htmlFor="year">Año</label>
          <input
            id="year"
            name="year"
            type="number"
            value={this.model.getField("year")}
            onChange={this.onChange}
            autoComplete="true"
          />
          {this.model.getErrors("year")}
        </div>
        <div>
          <div className="generic-input">
            <label htmlFor="">
              Compositores
              <button className={styles.addComposer} onClick={this.showPicker}>
                Agregar compositor
              </button>
            </label>
            <ul className={styles.composers}>
              {this.state.data.song_composers.map((song_composer, i) => {
                return (
                  <Composer
                    id={i}
                    toggle={this.toggleComposer}
                    songComposer={song_composer}
                  />
                );
              })}
            </ul>
          </div>
          <Modal
            show={this.state.showPicker}
            close={this.hidePicker}
            pick={this.pickArtist}
            title={
              <div>
                Seleccion de{" "}
                <OptionSelector
                  value={this.state.composerType}
                  options={composerTypes}
                  select={this.changeComposerType}
                />
                <button style={{ float: "right" }} onClick={this.hidePicker}>
                  <i className="fas fa-times" />
                </button>
              </div>
            }
          >
            {this.state.composerType.value === "Artist" && (
              <ArtistPicker pick={this.pickComposer} cancel={this.hidePicker} />
            )}
            {this.state.composerType.value === "Band" && (
              <BandPicker pick={this.pickComposer} cancel={this.hidePicker} />
            )}
          </Modal>
        </div>

        <div>
          <button onClick={this.submit}>{this.props.submitLabel}</button>
          <button onClick={this.cancel}>{this.props.cancelLabel}</button>
        </div>
      </form>
    );
  }
}

class Composer extends Component {
  toggle = () => {
    this.props.toggle(this.props.id);
  };
  render = () => {
    const { songComposer } = this.props;
    if (!songComposer.composer) return null;
    return (
      <li onClick={this.toggle}>
        <span
          style={
            songComposer._destroy ? { textDecoration: "line-through" } : {}
          }
        >
          {songComposer.composer.artistic_name}
        </span>
        <i
          style={{ marginLeft: 5 }}
          className={songComposer._destroy ? "fas fa-times" : "fas fa-check"}
        />
      </li>
    );
  };
}

export default SongForm;
