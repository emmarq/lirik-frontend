import { url } from "../../util/variables";

export const getSongs = queryPart => {
  return fetch(url(`/api/songs?${queryPart}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    return response.json().then(error => Promise.reject(error.message));
  });
};

export const getSong = songId => {
  return fetch(url(`/api/songs/${songId}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    else return Promise.reject("No hay cancion");
  });
};

export const createSong = song => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/songs`), {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(song)
  }).then(response => {
    if (response.status === 200) return response.json();
    return response.json().then(error => Promise.reject(error.message));
  });
};

export const updateSong = song => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/songs/${song.id}`), {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(song)
  }).then(response => {
    if (response.status === 200) return response.json();
    return response.json().then(error => Promise.reject(error.message));
  });
};

export const deleteSong = songId => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/songs/${songId}`), {
    method: "DELETE"
  });
};
