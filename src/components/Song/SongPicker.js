import React, { Component } from "react";
import ArtistPicker from "../Artist/ArtistPicker";
import BandPicker from "../Band/BandPicker";
import {
	Modal,
	OptionSelector,
	Suggestions,
	Action,
	Picker
} from "../../util/components";
import { FormModel, notEmpty } from "../../util/FormModel";
import { getSongs, createSong, updateSong } from "./Service";
import message from "../Message/state";
import styles from "./song_picker.module.css";
import { withTranslation } from "react-i18next";

class SongPicker extends Picker {
	constructor(props) {
		super(props);
		this.model = new FormModel(this);
		this.model.addValidation("name", notEmpty, props.t("which_name"));
		this.model.addValidation("year", notEmpty, props.t("which_year"));
		this.composerTypes = [
			{ value: "Artist", label: props.t("artist") },
			{ value: "Band", label: props.t("band") }
		];
		this.state = {
			form: false,
			composerType: this.composerTypes[0],
			data: { name: "", year: "", image: "", song_composers: [] },
			errors: {},
			songs: []
		};
		//this.model.addValidation("image", notEmpty, "Especifique una imagen");
	}

	showComposerPicker = () => {
		this.setState(state => ({ ...state, showPicker: true }));
	};
	hideComposerPicker = () => {
		this.setState(state => ({ ...state, showPicker: false }));
	};
	pickComposer = composer => {
		this.setState(state => {
			const nextState = { ...this.state };
			nextState.showPicker = false;
			for (let ac of nextState.data.song_composers) {
				if (
					ac.song_composer_id === composer.id &&
					ac.song_composer_type === state.composerType.value
				)
					return nextState;
			}
			nextState.data.song_composers = [
				...nextState.data.song_composers,
				{
					composer: composer,
					composer_id: composer.id,
					composer_type: state.composerType.value
				}
			];
			return nextState;
		});
	};
	changeComposerType = composerType => {
		this.setState(state => ({ ...state, composerType: composerType }));
	};
	toggleComposer = index => {
		this.setState(state => {
			const nextState = { ...state };
			if (nextState.data.song_composers[index].id)
				nextState.data.song_composers[index]._destroy = !nextState.data
					.song_composers[index]._destroy;
			else nextState.data.song_composers.splice(index, 1);
			return nextState;
		});
	};
	onChange = e => {
		if (e.target.id === "image") {
			this.model.setField("image", e.target.value);
			return;
		}
		if (e.target.id === "name" && !this.state.form) {
			const name = e.target.value;
			clearTimeout(this.timeout);
			this.timeout = setTimeout(() => {
				if (!name) {
					this.setState(state => ({ ...state, songs: [] }));
					return;
				}
				getSongs(`search=${name}&per_page=5`).then(page => {
					this.setState(state => {
						return { ...state, songs: page.entries };
					});
				});
			}, 250);
			//todo search song
		}
		this.model.setField(e.target.id, e.target.value);
	};
	submit = e => {
		e.preventDefault();
		if (this.model.isValid()) {
			const song = { ...this.model.getData() };

			const song_composers = [];

			for (let ac of song.song_composers) {
				const song_composer = { ...ac };
				delete song_composer.song;
				delete song_composer.composer;
				song_composers.push(song_composer);
			}

			song.song_composers_attributes = song_composers;
			delete song.song_composers;
			delete song.song_interpretations;

			const promiseSong = song.id ? updateSong(song) : createSong(song);

			promiseSong
				.then(song => {
					this.props.pick(song);
				})
				.catch(e => message.error(e));
		}
	};
	itemImage = item => {
		return item.image && <img src={item.image} alt={item.name} />;
	};

	itemMetadata = item => {
		return [<div>{item.name}</div>, <div>{item.release_year}</div>];
	};
	itemActions = item => {
		return (
			<Action
				fn={this.editSuggestion}
				label={<i className="fas fa-edit" />}
				item={item}
			/>
		);
	};
	render = () => {
		return (
			<div>
				{this.modeSwitcher()}
				<div className="generic-input">
					<label htmlFor="name">{this.props.t("name")}</label>
					<input
						ref={this.input}
						id="name"
						type="text"
						value={this.model.getField("name")}
						onChange={this.onChange}
					/>
					{this.model.getErrors("name")}
				</div>
				{this.state.form ? (
					<FormPart
						t={this.props.t}
						composerTypes={this.composerTypes}
						model={this.model}
						onChange={this.onChange}
						submit={this.submit}
						cancel={this.cancel}
						songComposers={this.state.data.song_composers}
						toggleComposer={this.toggleComposer}
						showComposerPicker={this.showComposerPicker}
						showPicker={this.state.showPicker}
						changeComposerType={this.changeComposerType}
						composerType={this.state.composerType}
						pickSongComposer={this.pickComposer}
						hideComposerPicker={this.hideComposerPicker}
					/>
				) : (
					<Suggestions
						items={this.state.songs}
						pick={this.props.pick}
						k="id"
						image={this.itemImage}
						metadata={this.itemMetadata}
						actions={this.itemActions}
					/>
				)}
			</div>
		);
	};
}

const FormPart = props => {
	const {
		t,
		model,
		onChange,
		submit,
		cancel,
		composerTypes,
		songComposers,
		toggleComposer,
		showComposerPicker,
		changeComposerType,
		composerType,
		pickSongComposer,
		hideComposerPicker,
		showPicker
	} = props;
	return (
		<div>
			<div className="generic-input">
				<label htmlFor="year">{t("year")}</label>
				<input
					id="year"
					type="number"
					value={model.getField("year")}
					onChange={onChange}
				/>
				{model.getErrors("year")}
			</div>
			<div className="generic-input">
				<label htmlFor="">
					{t("composers")}
					<button
						className={styles.addComposer}
						onClick={showComposerPicker}
					>
						{t("add_composer")}
					</button>
				</label>
				<ul className={styles.composers}>
					{songComposers.map((song_composer, i) => {
						return (
							<SongComposer
								key={song_composer.id}
								id={i}
								toggle={toggleComposer}
								songComposer={song_composer}
							/>
						);
					})}
				</ul>
				<Modal
					style={{ zIndex: 2 }}
					show={showPicker}
					close={hideComposerPicker}
					title={
						<div>
							{t("selection_of")}
							<OptionSelector
								value={composerType}
								options={composerTypes}
								select={changeComposerType}
							/>
							<button
								style={{ float: "right" }}
								onClick={hideComposerPicker}
							>
								<i className="fas fa-times" />
							</button>
						</div>
					}
				>
					{composerType.value === "Artist" && (
						<ArtistPicker
							pick={pickSongComposer}
							cancel={hideComposerPicker}
						/>
					)}
					{composerType.value === "Band" && (
						<BandPicker
							pick={pickSongComposer}
							cancel={hideComposerPicker}
						/>
					)}
				</Modal>
			</div>
			<div>
				<div>
					<button onClick={submit}>
						{model.getField("id")
							? t("edit_pick")
							: t("create_pick")}
					</button>
					<button onClick={cancel}>{t("cancel")}</button>
				</div>
			</div>
		</div>
	);
};

class SongComposer extends Component {
	toggle = () => {
		this.props.toggle(this.props.id);
	};
	render = () => {
		const { songComposer } = this.props;
		if (!songComposer.composer) return null;
		return (
			<li onClick={this.toggle}>
				<span
					style={
						songComposer._destroy
							? { textDecoration: "line-through" }
							: {}
					}
				>
					{songComposer.composer.artistic_name}
				</span>
				<i
					style={{ marginLeft: 5 }}
					className={
						songComposer._destroy ? "fas fa-times" : "fas fa-check"
					}
				/>
			</li>
		);
	};
}

export default withTranslation()(SongPicker);
