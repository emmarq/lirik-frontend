import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getSong } from "../Service";
import styles from "./song.module.css";

class Song extends Component {
  constructor(props) {
    super(props);
    this.state = { fetched: false };
  }
  componentDidMount() {
    getSong(this.props.match.params.id)
      .then(song => {
        this.setState({ fetched: true, song: song });
      })
      .catch(e => {
        this.setState({ fetched: true });
      });
  }
  render() {
    const song = this.state.song;

    if (!this.state.fetched) return "wait";
    if (!song) return "not found";

    return (
      <div className={`${styles.layout} container`}>
        <div className="card">
          <div className="card-title">
            <h2>
              {song.name}
              <button style={{ float: "right" }} onClick={this.hidePicker}>
                <i className="far fa-trash-alt" />
              </button>
              <Link
                style={{ float: "right" }}
                to={`${this.props.match.url}/edit`}
              >
                <i className="fas fa-edit" />
              </Link>
            </h2>
          </div>
          <div className="card-body">
            {song.year}
            <Link to={`/songs/${song.id}/new-interpretation`}>
              Nueva interpretación
            </Link>
          </div>
        </div>
        <div className="card">
          <div className="card-title">Interpretaciones</div>
          <div className="card-body">
            <ul>
              {song.interpretations.map(interpretation => (
                <li key={interpretation.id}>
                  <h3>
                    <Link to={`/interpretations/${interpretation.id}`}>
                      {interpretation.name}
                    </Link>
                  </h3>
                  <p>
                    {interpretation.artist} <br />
                    {interpretation.description}
                    <br />
                    {interpretation.year}
                  </p>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Song;
