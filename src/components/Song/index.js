import Songs from "./Songs/index";
import Song from "./Song/index";
import NewSong from "./NewSong/index";
import EditSong from "./EditSong/index";

export { EditSong, NewSong, Song, Songs };
