import React, { Component } from "react";
import SongForm from "../SongForm";
import { createSong } from "../Service";
import styles from "./new-song.module.css";
import message from "../../Message/state";

class NewSong extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.createSong = this.createSong.bind(this);
    this.cancel = this.cancel.bind(this);
  }
  createSong(song) {
    createSong(song)
      .then(created => {
        message.success("Canción creada");
        this.props.history.push(`/songs/${created.id}`);
      })
      .catch(e => {
        message.error(e);
      });
  }
  cancel() {
    this.props.history.push(`/songs`);
  }
  render() {
    return (
      <div className={`${styles.newSong} container card`}>
        <div className="card-title">
          <h2>Nueva canción</h2>
        </div>
        <div className="card-body">
          <SongForm
            header="Información de la canción"
            submit={this.createSong}
            cancel={this.cancel}
            submitLabel="Añadir canción"
            cancelLabel="Cancelar"
          />
        </div>
      </div>
    );
  }
}

export default NewSong;
