import { url } from "../../util/variables";

export const getArtists = queryPart => {
  return fetch(url(`/api/artists?${queryPart}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    return response.json().then(error => Promise.reject(error.message));
  });
};

export const getArtist = artistId => {
  return fetch(url(`/api/artists/${artistId}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    else return Promise.reject("El artista no existe");
  });
};

export const createArtist = artist => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/artists`), {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(artist)
  }).then(response => {
    if (response.status === 200) return response.json();
    else return response.json().then(error => Promise.reject(error.message));
  });
};

export const updateArtist = artist => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/artists/${artist.id}`), {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(artist)
  }).then(response => {
    if (response.status === 200) return response.json();
    else return response.json().then(error => Promise.reject(error.message));
  });
};

export const deleteArtist = artistId => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/artists/${artistId}`), {
    method: "DELETE"
  });
};
