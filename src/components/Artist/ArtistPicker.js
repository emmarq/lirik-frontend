import React from "react";
import { Suggestions, Action, Picker, ImageInput } from "../../util/components";
import { FormModel, notEmpty } from "../../util/FormModel";
import { getArtists, createArtist, updateArtist } from "./Service";
import message from "../Message/state";
import { withTranslation } from "react-i18next";

class ArtistPicker extends Picker {
	constructor(props) {
		super(props);
		this.model = new FormModel(this);
		this.model.addValidation("name", notEmpty, "¿Cual es su nombre?");
		this.model.addValidation(
			"artistic_name",
			notEmpty,
			props.t("which_artistic_name")
		);
		this.model.addValidation(
			"birth_date",
			notEmpty,
			props.t("when_birth_date")
		);
		//this.model.addValidation("image", notEmpty, "Especifique una imagen");
	}
	state = {
		form: false,
		data: { name: "", artistic_name: "", birth_date: "", image: "" },
		errors: {},
		artists: []
	};
	onChange = e => {
		if (e.target.id === "image") {
			this.model.setField("image", e.target.value);
			return;
		}
		if (e.target.id === "name" && !this.state.form) {
			const name = e.target.value;
			clearTimeout(this.timeout);
			this.timeout = setTimeout(() => {
				if (!name) {
					this.setState(state => ({ ...state, artists: [] }));
					return;
				}
				getArtists(`search=${name}&per_page=5`).then(page => {
					this.setState(state => {
						return { ...state, artists: page.entries };
					});
				});
			}, 250);
			//todo search artist
		}
		this.model.setField(e.target.id, e.target.value);
	};
	submit = e => {
		e.preventDefault();
		if (this.model.isValid()) {
			const artist = { ...this.model.getData() };
			delete artist.song_composers;
			delete artist.songs;
			delete artist.album_creators;
			delete artist.albums;
			delete artist.interpretation_performers;
			delete artist.interpretation;

			const promiseArtist = artist.id
				? updateArtist(artist)
				: createArtist(artist);

			promiseArtist
				.then(artist => {
					this.props.pick(artist);
				})
				.catch(e => message.error(e));
		}
	};
	itemImage = item => {
		return item.image && <img src={item.image} alt={item.name} />;
	};

	itemMetadata = item => {
		return [<div>{item.artistic_name}</div>, <div>{item.birth_date}</div>];
	};
	itemActions = item => {
		return (
			<Action
				fn={this.editSuggestion}
				label={<i className="fas fa-edit" />}
				item={item}
			/>
		);
	};
	render = () => {
		return (
			<div>
				{this.modeSwitcher()}
				<div className="generic-input">
					<label htmlFor="name">{this.props.t("name")}</label>
					<input
						ref={this.input}
						id="name"
						type="text"
						value={this.model.getField("name")}
						onChange={this.onChange}
					/>
					{this.model.getErrors("name")}
				</div>
				{this.state.form ? (
					<FormPart
						t={this.props.t}
						model={this.model}
						onChange={this.onChange}
						submit={this.submit}
						cancel={this.cancel}
					/>
				) : (
					<Suggestions
						items={this.state.artists}
						pick={this.props.pick}
						k="id"
						image={this.itemImage}
						metadata={this.itemMetadata}
						actions={this.itemActions}
					/>
				)}
			</div>
		);
	};
}

const FormPart = props => {
	const { model, onChange, submit, cancel, t } = props;
	return (
		<div>
			<div className="generic-input">
				<label htmlFor="artistic_name">{t("artistic_name")}</label>
				<input
					id="artistic_name"
					type="text"
					value={model.getField("artistic_name")}
					onChange={onChange}
				/>
				{model.getErrors("artistic_name")}
			</div>
			<div className="generic-input">
				<label htmlFor="birth_date">{t("birth_date")}</label>
				<input
					id="birth_date"
					type="date"
					value={model.getField("birth_date")}
					onChange={onChange}
				/>
				{model.getErrors("birth_date")}
			</div>
			<ImageInput id="image" onChange={onChange} model={model} />
			<div>
				<div>
					<button onClick={submit}>
						{model.getField("id")
							? t("edit_pick")
							: t("create_pick")}
					</button>
					<button onClick={cancel}>{t("cancel")}</button>
				</div>
			</div>
		</div>
	);
};

export default withTranslation()(ArtistPicker);
