import React, { Component } from "react";
import ArtistPicker from "../Artist/ArtistPicker";
import BandPicker from "../Band/BandPicker";
import {
	Modal,
	OptionSelector,
	Suggestions,
	Action,
	Picker,
	ImageInput
} from "../../util/components";
import { FormModel, notEmpty } from "../../util/FormModel";
import { getAlbums, createAlbum, updateAlbum } from "./Service";
import message from "../Message/state";
import styles from "./album_picker.module.css";
import { withTranslation } from "react-i18next";

class AlbumPicker extends Picker {
	constructor(props) {
		super(props);
		this.model = new FormModel(this);
		this.model.addValidation("name", notEmpty, props.t("which_name"));
		this.model.addValidation(
			"release_year",
			notEmpty,
			props.t("which_release_year")
		);
		this.creatorTypes = [
			{ value: "Artist", label: props.t("artist") },
			{ value: "Band", label: props.t("band") }
		];
		this.state = {
			form: false,
			creatorType: this.creatorTypes[0],
			data: { name: "", release_year: "", image: "", album_creators: [] },
			errors: {},
			albums: []
		};
		//this.model.addValidation("image", notEmpty, "Especifique una imagen");
	}

	showCreatorPicker = () => {
		this.setState(state => ({ ...state, showPicker: true }));
	};
	hideCreatorPicker = () => {
		this.setState(state => ({ ...state, showPicker: false }));
	};
	pickCreator = creator => {
		this.setState(state => {
			const nextState = { ...this.state };
			nextState.showPicker = false;
			for (let ac of nextState.data.album_creators) {
				if (
					ac.album_creator_id === creator.id &&
					ac.album_creator_type === state.creatorType.value
				)
					return nextState;
			}
			nextState.data.album_creators = [
				...nextState.data.album_creators,
				{
					album_creator: creator,
					album_creator_id: creator.id,
					album_creator_type: state.creatorType.value
				}
			];
			return nextState;
		});
	};
	changeCreatorType = creatorType => {
		this.setState(state => ({ ...state, creatorType: creatorType }));
	};
	toggleCreator = index => {
		this.setState(state => {
			const nextState = { ...state };
			if (nextState.data.album_creators[index].id)
				nextState.data.album_creators[index]._destroy = !nextState.data
					.album_creators[index]._destroy;
			else nextState.data.album_creators.splice(index, 1);
			return nextState;
		});
	};
	onChange = e => {
		if (e.target.id === "image") {
			this.model.setField("image", e.target.value);
			return;
		}
		if (e.target.id === "name" && !this.state.form) {
			const name = e.target.value;
			clearTimeout(this.timeout);
			this.timeout = setTimeout(() => {
				if (!name) {
					this.setState(state => ({ ...state, albums: [] }));
					return;
				}
				getAlbums(`search=${name}&per_page=5`).then(page => {
					this.setState(state => {
						return { ...state, albums: page.entries };
					});
				});
			}, 250);
			//todo search album
		}
		this.model.setField(e.target.id, e.target.value);
	};
	submit = e => {
		e.preventDefault();
		if (this.model.isValid()) {
			const album = { ...this.model.getData() };

			const album_creators = [];

			for (let ac of album.album_creators) {
				const album_creator = { ...ac };
				delete album_creator.album;
				delete album_creator.album_creator;
				album_creators.push(album_creator);
			}

			album.album_creators_attributes = album_creators;
			delete album.album_creators;
			delete album.album_interpretations;

			const promiseAlbum = album.id
				? updateAlbum(album)
				: createAlbum(album);

			promiseAlbum
				.then(album => {
					this.props.pick(album);
				})
				.catch(e => message.error(e));
		}
	};
	itemImage = item => {
		return item.image && <img src={item.image} alt={item.name} />;
	};

	itemMetadata = item => {
		return [<div>{item.name}</div>, <div>{item.release_year}</div>];
	};
	itemActions = item => {
		return (
			<Action
				fn={this.editSuggestion}
				label={<i className="fas fa-edit" />}
				item={item}
			/>
		);
	};
	render = () => {
		return (
			<div>
				{this.modeSwitcher()}
				<div className="generic-input">
					<label htmlFor="name">{this.props.t("name")}</label>
					<input
						ref={this.input}
						id="name"
						type="text"
						value={this.model.getField("name")}
						onChange={this.onChange}
					/>
					{this.model.getErrors("name")}
				</div>
				{this.state.form ? (
					<FormPart
						t={this.props.t}
						creatorTypes={this.creatorTypes}
						model={this.model}
						onChange={this.onChange}
						submit={this.submit}
						cancel={this.cancel}
						albumCreators={this.state.data.album_creators}
						toggleCreator={this.toggleCreator}
						showCreatorPicker={this.showCreatorPicker}
						showPicker={this.state.showPicker}
						changeCreatorType={this.changeCreatorType}
						creatorType={this.state.creatorType}
						pickAlbumCreator={this.pickCreator}
						hideCreatorPicker={this.hideCreatorPicker}
					/>
				) : (
					<Suggestions
						items={this.state.albums}
						pick={this.props.pick}
						k="id"
						image={this.itemImage}
						metadata={this.itemMetadata}
						actions={this.itemActions}
					/>
				)}
			</div>
		);
	};
}

const FormPart = props => {
	const {
		t,
		model,
		onChange,
		submit,
		cancel,
		albumCreators,
		toggleCreator,
		showCreatorPicker,
		changeCreatorType,
		creatorTypes,
		creatorType,
		pickAlbumCreator,
		hideCreatorPicker,
		showPicker
	} = props;
	return (
		<div>
			<div className="generic-input">
				<label htmlFor="release_year">{t("release_year")}</label>
				<input
					id="release_year"
					type="number"
					value={model.getField("release_year")}
					onChange={onChange}
				/>
				{model.getErrors("release_year")}
			</div>
			<ImageInput id="image" onChange={onChange} model={model} />
			<div className="generic-input">
				<label htmlFor="">
					{t("album_creators")}
					<button
						className={styles.addCreator}
						onClick={showCreatorPicker}
					>
						{t("add_album_creator")}
					</button>
				</label>
				<ul className={styles.creators}>
					{albumCreators.map((album_creator, i) => {
						return (
							<AlbumCreator
								key={album_creator.id}
								id={i}
								toggle={toggleCreator}
								albumCreator={album_creator}
							/>
						);
					})}
				</ul>
				<Modal
					style={{ zIndex: 2 }}
					show={showPicker}
					close={hideCreatorPicker}
					title={
						<div>
							{t("selection_of")}
							<OptionSelector
								value={creatorType}
								options={creatorTypes}
								select={changeCreatorType}
							/>
							<button
								style={{ float: "right" }}
								onClick={hideCreatorPicker}
							>
								<i className="fas fa-times" />
							</button>
						</div>
					}
				>
					{creatorType.value === "Artist" && (
						<ArtistPicker
							pick={pickAlbumCreator}
							cancel={hideCreatorPicker}
						/>
					)}
					{creatorType.value === "Band" && (
						<BandPicker
							pick={pickAlbumCreator}
							cancel={hideCreatorPicker}
						/>
					)}
				</Modal>
			</div>
			<div>
				<div>
					<button onClick={submit}>
						{model.getField("id")
							? t("edit_pick")
							: t("create_pick")}
					</button>
					<button onClick={cancel}>{t("cancel")}</button>
				</div>
			</div>
		</div>
	);
};

class AlbumCreator extends Component {
	toggle = () => {
		this.props.toggle(this.props.id);
	};
	render = () => {
		const { albumCreator } = this.props;
		if (!albumCreator.album_creator) return null;
		return (
			<li onClick={this.toggle}>
				<span
					style={
						albumCreator._destroy
							? { textDecoration: "line-through" }
							: {}
					}
				>
					{albumCreator.album_creator.artistic_name}
				</span>
				<i
					style={{ marginLeft: 5 }}
					className={
						albumCreator._destroy ? "fas fa-times" : "fas fa-check"
					}
				/>
			</li>
		);
	};
}

export default withTranslation()(AlbumPicker);
