import { url } from "../../util/variables";

export const getAlbums = queryPart => {
  return fetch(url(`/api/albums?${queryPart}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    return response.json().then(error => Promise.reject(error.message));
  });
};

export const getAlbum = albumId => {
  return fetch(url(`/api/albums/${albumId}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    else return Promise.reject("El albuma no existe");
  });
};

export const createAlbum = album => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/albums`), {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(album)
  }).then(response => {
    if (response.status === 200) return response.json();
    else return response.json().then(error => Promise.reject(error.message));
  });
};

export const updateAlbum = album => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/albums/${album.id}`), {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(album)
  }).then(response => {
    if (response.status === 200) return response.json();
    else return response.json().then(error => Promise.reject(error.message));
  });
};

export const deleteAlbum = albumId => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/albums/${albumId}`), {
    method: "DELETE"
  });
};
