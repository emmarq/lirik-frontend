import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { Songs, Song, EditSong } from "../Song";
import { Interpretations, InterpretationMaster } from "../Interpretation";
import { basename, origin } from "../../util/variables";
import queryString from "query-string";
import AuthButtons from "../Auth/AuthButtons";
import { checkJWT } from "../Auth/Service";
import styles from "./app.module.css";
import { withTranslation } from "react-i18next";

class App extends Component {
  state = {};
  logout = () => {
    localStorage.removeItem("jwt");
    this.setState({ isAuthenticated: false, token: "", user: null });
  };
  onFailure = () => {};
  onAuth = jwt => {
    this.setState({ isAuthenticated: true, token: jwt.token, user: jwt.user });
  };
  componentDidMount() {
    checkJWT()
      .then(user => {
        if (!this.unmount) this.setState({ isAuthenticated: true, user: user });
      })
      .catch(e => {
        if (!this.unmount) this.setState({ isAuthenticated: false });
      });
  }
  componentWillUnmount() {
    this.unmount = true;
  }
  render() {
    let userStatus;
    if (this.state.hasOwnProperty("isAuthenticated")) {
      if (this.state.isAuthenticated) {
        userStatus = [
          <li className={styles.userName}>
            <div>{this.state.user.current_provider.name}</div>
            <small onClick={this.logout}>salir</small>
          </li>,
          <li>
            <img alt="user" src={this.state.user.current_provider.image} />
          </li>
        ];
      } else {
        userStatus = (
          <li>
            <AuthButtons onFailure={this.onFailure} onAuth={this.onAuth} />
          </li>
        );
      }
    } else userStatus = "wait";

    const { t } = this.props;

    return (
      <Router basename={basename}>
        <div className={styles.layout}>
          <nav className={styles.header}>
            <h1 className={styles.logo}>
              <Link to="/">LIRIK</Link>
            </h1>
            <ul className={styles.list}>
              <li>
                <Link to="/interpretations/new/edit">{t("create")}</Link>
              </li>
            </ul>
            <ul className={styles.user}>{userStatus}</ul>
          </nav>
          <section className={styles.content}>
            <Switch>
              <Route path="/songs" component={SongRoutes} />
              <Route path="/interpretations" component={InterpretationRoutes} />
              <Route path="/auth" component={AuthCallbacksRoutes} />
              <Route path="/" component={Interpretations} />
            </Switch>
          </section>
        </div>
      </Router>
    );
  }
}

const PaypalForm = props => (
  <form
    action="https://www.paypal.com/cgi-bin/webscr"
    method="post"
    target="_top"
  >
    <input type="hidden" name="cmd" value="_s-xclick" />
    <input type="hidden" name="hosted_button_id" value="L7UQ7LAV8T3SQ" />
    <input
      type="image"
      src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif"
      border="0"
      name="submit"
      title="PayPal - The safer, easier way to pay online!"
      alt="Donate with PayPal button"
    />
    <img
      alt=""
      border="0"
      src="https://www.paypal.com/en_CO/i/scr/pixel.gif"
      width="1"
      height="1"
    />
  </form>
);

const callOnce = cb => {
  let called = false;
  return props => {
    if (!called) {
      cb(props);
      called = true;
    }
  };
};

const catchFacebook = callOnce(({ location }) => {
  const data = queryString.parse(location.search.slice(1));
  data.provider = "facebook";
  window.opener.postMessage(data, origin);
  window.close();
});

const catchGoogle = callOnce(({ location }) => {
  const data = queryString.parse(location.search.slice(1));
  data.provider = "google";
  window.opener.postMessage(data, origin);
  window.close();
});

const AuthCallbacksRoutes = props => {
  return (
    <Switch>
      <Route
        path={`${props.match.path}/facebook`}
        render={props => {
          catchFacebook(props);
        }}
      />
      <Route
        path={`${props.match.path}/google`}
        render={props => {
          catchGoogle(props);
        }}
      />
      <Route path={`${props.match.path}/twitter`} render={() => {}} />
    </Switch>
  );
};

const SongRoutes = props => (
  <Switch>
    <Route path={`${props.match.path}/:id/edit`} component={EditSong} />
    <Route path={`${props.match.path}/:id`} component={Song} />
    <Route path={`${props.match.path}`} component={Songs} />
  </Switch>
);

const InterpretationRoutes = props => (
  <Switch>
    <Route path={`${props.match.path}/:id`} component={InterpretationMaster} />
    <Route path={`${props.match.path}`} component={Interpretations} />
  </Switch>
);

export default withTranslation()(App);
