import React from "react";
import { Suggestions, Action, Picker, ImageInput } from "../../util/components";
import { FormModel, notEmpty } from "../../util/FormModel";
import { getBands, createBand, updateBand } from "./Service";
import message from "../Message/state";
import { withTranslation } from "react-i18next";

class BandPicker extends Picker {
	constructor(props) {
		super(props);
		this.model = new FormModel(this);
		this.model.addValidation("name", notEmpty, props.t("which_name"));
		this.model.addValidation(
			"artistic_name",
			notEmpty,
			props.t("which_artistic_name")
		);
		this.model.addValidation("since", notEmpty, props.t("when_begin"));
		//this.model.addValidation("image", notEmpty, "Especifique una imagen");
	}
	input = React.createRef();
	state = {
		form: false,
		data: { name: "", artistic_name: "", since: "", until: "", image: "" },
		errors: {},
		bands: []
	};
	onChange = e => {
		if (e.target.id === "image") {
			//todo upload image to cdn
			this.model.setField("image", "");
			return;
		}
		if (e.target.id === "name" && !this.state.form) {
			const name = e.target.value;
			clearTimeout(this.timeout);
			this.timeout = setTimeout(() => {
				if (!name) {
					this.setState(state => ({ ...state, bands: [] }));
					return;
				}
				getBands(`search=${name}&per_page=5`).then(page => {
					this.setState(state => {
						return { ...state, bands: page.entries };
					});
				});
			}, 250);
		}
		this.model.setField(e.target.id, e.target.value);
	};
	submit = e => {
		e.preventDefault();
		if (this.model.isValid()) {
			const band = { ...this.model.getData() };
			delete band.song_composers;
			delete band.songs;
			delete band.album_creators;
			delete band.albums;
			delete band.interpretation_performers;
			delete band.interpretation;

			const promiseBand = band.id ? updateBand(band) : createBand(band);

			promiseBand
				.then(band => {
					this.props.pick(band);
				})
				.catch(e => message.error(e));
		}
	};
	itemImage = item => {
		return item.image && <img src={item.image} alt={item.name} />;
	};

	itemMetadata = item => {
		return [
			<div>{item.name}</div>,
			<div>{item.artistic_name}</div>,
			<div>{item.since}</div>,
			<div>{item.until}</div>
		];
	};
	itemActions = item => {
		return (
			<Action
				fn={this.editSuggestion}
				label={<i className="fas fa-edit" />}
				item={item}
			/>
		);
	};
	render = () => {
		return (
			<div>
				{this.modeSwitcher()}
				<div className="generic-input">
					<label htmlFor="name">{this.props.t("name")}</label>
					<input
						ref={this.input}
						id="name"
						type="text"
						value={this.model.getField("name")}
						onChange={this.onChange}
					/>
					{this.model.getErrors("name")}
				</div>
				{this.state.form ? (
					<FormPart
						t={this.props.t}
						model={this.model}
						onChange={this.onChange}
						submit={this.submit}
						cancel={this.cancel}
					/>
				) : (
					<Suggestions
						items={this.state.bands}
						pick={this.props.pick}
						k="id"
						image={this.itemImage}
						metadata={this.itemMetadata}
						actions={this.itemActions}
					/>
				)}
			</div>
		);
	};
}

const FormPart = props => {
	const { model, onChange, submit, cancel, t } = props;
	return (
		<div>
			<div className="generic-input">
				<label htmlFor="artistic_name">{t("artistic_name")}</label>
				<input
					id="artistic_name"
					type="text"
					value={model.getField("artistic_name")}
					onChange={onChange}
				/>
				{model.getErrors("artistic_name")}
			</div>
			<div className="generic-input">
				<label htmlFor="since">{t("band_since")}</label>
				<input
					id="since"
					type="date"
					value={model.getField("since")}
					onChange={onChange}
				/>
				{model.getErrors("since")}
			</div>
			<div className="generic-input">
				<label htmlFor="until">{t("band_until")}</label>
				<input
					id="until"
					type="date"
					value={model.getField("until")}
					onChange={onChange}
				/>
				{model.getErrors("until")}
			</div>
			<ImageInput id="image" onChange={onChange} model={model} />
			<div>
				<div>
					<button onClick={submit}>
						{model.getField("id")
							? t("edit_pick")
							: t("create_pick")}
					</button>
					<button onClick={cancel}>{t("cancel")}</button>
				</div>
			</div>
		</div>
	);
};

export default withTranslation()(BandPicker);
