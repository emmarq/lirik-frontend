import { url } from "../../util/variables";

export const getBands = queryPart => {
  return fetch(url(`/api/bands?${queryPart}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    return response.json().then(error => Promise.reject(error.message));
  });
};

export const getBand = bandId => {
  return fetch(url(`/api/bands/${bandId}`), {
    headers: { Accept: "application/json" }
  }).then(response => {
    if (response.status === 200) return response.json();
    else return Promise.reject("La banda no existe");
  });
};

export const createBand = band => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/bands`), {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(band)
  }).then(response => {
    if (response.status === 200) return response.json();
    else return response.json().then(error => Promise.reject(error.message));
  });
};

export const updateBand = band => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/bands/${band.id}`), {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${jwt}`
    },
    body: JSON.stringify(band)
  }).then(response => {
    if (response.status === 200) return response.json();
    else return response.json().then(error => Promise.reject(error.message));
  });
};

export const deleteBand = bandId => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) return Promise.reject("No hay sesión iniciada");
  return fetch(url(`/api/bands/${bandId}`), {
    method: "DELETE"
  });
};
