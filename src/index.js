import React from "react";
import ReactDOM from "react-dom";
import "./styles/styles.scss";
import "./i18n";
import { App } from "./components";
import * as serviceWorker from "./serviceWorker";
import Messages from "./components/Message/Message";
import message from "./components/Message/state";

ReactDOM.render(<App />, document.getElementById("root"));

message.subscribe(messages => {
	ReactDOM.render(
		<Messages messages={messages} />,
		document.getElementById("messages")
	);
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
