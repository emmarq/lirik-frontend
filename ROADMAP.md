#v1.0

- actualizar wait por loader
- Compartir lapsos de letra
- Agregar datos de uso para el filtro de canciones (mas vistos, mas populares, mejor valorados, ultimos)
- Agregar thumbnails a las canciones
- Agregar licencia de uso y politicas de uso de datos
- poner album en la interpretacion
- poner sugerencias de canciones al crear la nueva interpretacion
- crear la cancion al crear la interpretacion
- arreglar vista de la cancion y sus interpretaciones

#v2.0

- busqueda en la barra de titulo
- web workers
- internacionalizacion
- style overhaull (tdb)

#v3.0

- multiples letras en una misma cancion